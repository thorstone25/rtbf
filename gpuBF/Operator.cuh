/**
 @file gpuBF/Operator.cuh
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2019-03-03

Copyright 2019 Dongwoon Hyun

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef RTBF_OPERATOR_CUH_
#define RTBF_OPERATOR_CUH_

#include "Tensor.cuh"
#include "gpuBF.cuh"

namespace rtbf {

/** @brief Abstract base class for processing data.

Operator is an abstract base class that is used to process some input and
produce some output, and is used to facilitate the execution of a
computational graph consisting of numerous daisy-chained Operators. Operators
can be assigned to CUDA streams for concurrent and asynchronous execution,
and also provide several convenient utilities, such as logging for errors,
warning, and information output using spdlog. Each Operator shares ownership
of its inputs and outputs via shared_ptr's, ensuring that these objects will
remain valid and accessible throughout the lifetime of the Operator, even if
previous or subsequent Operators in the graph have been destroyed.

Following the Resource Acquisition is Initialization (RAII) programming
idiom, all memory resources are allocated upon construction and freed upon
destruction. RAII makes it so that the resources allocated by an Operator are
coupled to the lifetime of the Operator object itself, reducing unsafe access
to dangling pointers. Any time-intensive tasks that can be performed a priori
should take place during construction (e.g., allocating any internal data
structures and pre-computing information where possible). During the
real-time loop, the Operator should provide an execution step that performs
high-throughput low-latency processing.

Note: Changing an Operator's inputs and outputs changes the shared_ptr
reference counts (an atomic operation), and should thus be considered a
"heavy" operation to be avoided. Hopefully for most use-cases, the
computational graph is unlikely to change during execution.
*/
template <typename T_in, typename T_out>
class Operator {
 protected:
  std::shared_ptr<T_in> in;    ///< Input
  std::shared_ptr<T_out> out;  ///< Output
  cudaStream_t stream;         ///< The CUDA stream for real-time execution
  std::string label;           ///< Name of Operator
  std::string logName;         ///< Name of spdlog logger
  std::shared_ptr<spdlog::logger> slog;  ///< Shared pointer to spdlog logger
  /// @brief Default constructor is protected to prevent instantiation.
  Operator() {}

 public:
  /// @brief Move-assignment operator
  Operator<T_in, T_out> &operator=(Operator<T_in, T_out> &&A) {
    std::swap(in, A.in);
    std::swap(out, A.out);
    std::swap(stream, A.stream);
    std::swap(label, A.label);
    std::swap(logName, A.logName);
    std::swap(slog, A.slog);
    return *this;
  }
  /// @brief Destructor
  virtual ~Operator() {
    in = nullptr;   // Clear the shared_ptr to input data
    out = nullptr;  // Clear the shared_ptr to output data
    loginfo("Destroyed {}.", label);
  }
  /// @brief Set a new input object (via shared pointer)
  void setInput(std::shared_ptr<T_in> input) { in = input; }
  /// @brief Set a new output object (via shared pointer)
  void setOutput(std::shared_ptr<T_in> output) { out = output; }
  /// @brief Returns the input object
  std::shared_ptr<T_in> getInput() const { return in; }
  /// @brief Returns the output object
  std::shared_ptr<T_out> getOutput() const { return out; }
  /// @brief Returns the raw input object
  T_in *getInputRaw() const { return in.get(); }
  /// @brief Returns the raw output object
  T_out *getOutputRaw() const { return out.get(); }
  /// @brief Returns the CUDA stream for this object
  cudaStream_t getCudaStream() const { return stream; }
  /// @brief Returns the label for this object
  std::string getLabel() const { return label; }
  /// @brief Returns the logger name
  std::string getLogger() const { return logName; }
  /// @brief Initialize the spdlog logger
  void initLogger() {
    if (logName.empty()) {
      slog = spdlog::default_logger();
    } else {
      slog = spdlog::get(logName);
      if (slog == nullptr) {
        slog = spdlog::default_logger();
        logwarn("Requested logger name not found. Using default logger.");
      }
    }
  }
  /// @brief Set spdlog logger by name
  void setLogger(std::string loggerName) {
    logName = loggerName;
    initLogger();
  }
  /// @brief Set label for this object
  void setLabel(std::string moniker) { label = moniker; }
  /// @brief Error logging function that prepends Tensor information
  template <class except = std::runtime_error, typename FormatString,
            typename... Args>
  inline void logerror(const FormatString &fmt, Args &&...args) {
    auto str = "(" + label + ")" + ": " + fmt::format(fmt, args...);
    slog->error(str);
    throw except(str);
  }
  /// @brief Warning logging function that prepends Tensor information
  template <typename FormatString, typename... Args>
  inline void logwarn(const FormatString &fmt, Args &&...args) {
    slog->warn("(" + label + ")" + ": " + fmt::format(fmt, args...));
  }
  /// @brief Information logging function that prepends Tensor information
  template <typename FormatString, typename... Args>
  inline void loginfo(const FormatString &fmt, Args &&...args) {
    slog->info("(" + label + ")" + ": " + fmt::format(fmt, args...));
  }
  /// @brief Debug logging function that prepends Tensor information
  template <typename FormatString, typename... Args>
  inline void logdebug(const FormatString &fmt, Args &&...args) {
    slog->debug("(" + label + ")" + ": " + fmt::format(fmt, args...));
  }
};

}  // namespace rtbf

#endif /* RTBF_OPERATOR_CUH_ */
