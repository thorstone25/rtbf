/**
 @file test/test_Bmode.cu
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2019-08-18
*/

#include "Bmode.cuh"
#include "gtest/gtest.h"

namespace rtbf {
namespace BmodeTest {
namespace cstd = cuda::std;

// Define dummy classes that will aid in running typed and parameterized tests
// on the CPU and GPU.
class CPU {};
class GPU {};
template <typename DeviceType>
int getDevice() {
  return -1;
}
template <>
int getDevice<GPU>() {
  return 0;
}
// We will perform typed tests using data types short and float, and execution
// on the CPU and GPU. (CPU and GPU are defined in test_gpuBF.cu.)
typedef std::pair<short, CPU> shortCPU;
typedef std::pair<short, GPU> shortGPU;
typedef std::pair<float, CPU> floatCPU;
typedef std::pair<float, GPU> floatGPU;
using MyTypes = ::testing::Types<shortCPU, shortGPU, floatCPU, floatGPU>;

// Use a templated test fixture to reuse resources between tests
template <typename Tpair>
class BmodeTest : public ::testing::Test {
 protected:
  using T = Tpair::first_type;   // Type
  using D = Tpair::second_type;  // Device
  // Re-use the same arrays for all tests
  const std::vector<size_t> dims = {3, 2, 1, 4};
  const size_t numel = prod(dims);
  std::vector<std::complex<T>> data;
  std::shared_ptr<Tensor<cstd::complex<T>>> Aptr;
  std::vector<float> env, envl, envp, envc, out;
  const float gamma = 0.3;
  int dev;

  // Set up the test suite
  BmodeTest() {
    spdlog::set_level(spdlog::level::debug);  // For more debugging output
    dev = getDevice<D>();
    data.resize(numel);
    out.resize(numel);
    // Create random input IQ data
    srand(time(NULL));
    for (size_t i = 0; i < numel; i++) {
      data[i].real(static_cast<T>((rand() % 16) * 14.f + 1));
      data[i].imag(static_cast<T>((rand() % 16) * 14.f + 1));
    }
    // Copy this data to a Tensor
    Aptr = std::make_shared<Tensor<cstd::complex<T>>>(dims, dev, "IQ data");
    Aptr->copyFrom(reinterpret_cast<cstd::complex<T> *>(data.data()));
    // Get the ground truth results
    env.resize(numel);                 // Envelope
    envl.resize(numel);                // Logarithmic compression
    envp.resize(numel);                // Power compression
    envc.resize(numel / dims.back());  // Compounding
    for (int i = 0; i < numel; i++) {
      env[i] = absf(data[i]);         // Raw envelope
      envl[i] = 20 * log10f(env[i]);  // Log-compressed envelope
      envp[i] = powf(env[i], gamma);  // Power-compressed envelope
    }
    // Also do envelope detection with incoherent compounding
    for (int i = 0; i < numel / dims.back(); i++) {
      envc[i] = 0.f;
      for (int j = 0; j < dims.back(); j++)
        envc[i] += absf(data[i + j * numel / dims.back()]);
    }
  }
  // Tear down the test suite
  ~BmodeTest() {}
};

TYPED_TEST_SUITE(BmodeTest, MyTypes);

TYPED_TEST(BmodeTest, EnvDetect) {
  using T = typename TypeParam::first_type;
  Bmode<cstd::complex<T>, float> B{Aptr};
  B.getEnvelope();
  B.getOutput()->copyTo(out.data());
  // Check result
  for (int i = 0; i < numel; i++) EXPECT_NEAR(env[i], out[i], 0.001);
}

TYPED_TEST(BmodeTest, EnvDetectLogComp) {
  using T = typename TypeParam::first_type;
  Bmode<cstd::complex<T>, float> B{Aptr};
  B.getEnvelopeLogCompress();
  B.getOutput()->copyTo(out.data());
  // Check result
  for (int i = 0; i < numel; i++) EXPECT_NEAR(envl[i], out[i], 0.001);
}

TYPED_TEST(BmodeTest, EnvDetectPowComp) {
  using T = typename TypeParam::first_type;
  Bmode<cstd::complex<T>, float> B{Aptr};
  B.getEnvelopePowCompress(gamma);
  B.getOutput()->copyTo(out.data());
  // Check result
  for (int i = 0; i < numel; i++) EXPECT_NEAR(envp[i], out[i], 0.001);
}
TYPED_TEST(BmodeTest, EnvDetectCompounding) {
  using T = typename TypeParam::first_type;
  Bmode<cstd::complex<T>, float> B{Aptr, dims.size() - 1};
  B.getEnvelope();
  B.getOutput()->copyTo(out.data());
  // Check result
  for (int i = 0; i < numel / dims.back(); i++)
    EXPECT_NEAR(envc[i], out[i], 0.001);
}

}  // namespace BmodeTest
}  // namespace rtbf