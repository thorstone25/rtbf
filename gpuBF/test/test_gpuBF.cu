/**
 @file test/test_gpuBF.cu
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2019-08-17
*/
#include <iostream>

#include "gtest/gtest.h"
#include "spdlog/sinks/rotating_file_sink.h"  // support for basic file logging
#include "spdlog/sinks/stdout_color_sinks.h"
#include "spdlog/spdlog.h"

int main(int argc, char** argv) {
  // Create basic file logger (not rotated)
  auto my_logger =
      spdlog::rotating_logger_mt("logger", "logs/basic.txt", 5242880, 1);
  auto sink = std::make_shared<spdlog::sinks::stdout_color_sink_mt>();
  my_logger->sinks().push_back(sink);
  spdlog::set_default_logger(my_logger);
  spdlog::set_pattern("[%Y-%m-%d %T.%e][%=7!l]%v");
  // spdlog::set_level(spdlog::level::debug);  // Use to output more information
  spdlog::set_level(spdlog::level::warn);  // Use to output less information
  ::testing::InitGoogleTest(&argc, argv);
  // spdlog::drop_all();
  return RUN_ALL_TESTS();
}
