/**
 @file gpuBF/InputLoader.cu
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2022-05-02

Copyright 2022 Dongwoon Hyun

Licensed under the Apache License, Version 2.0 (the "License");
you may not use ile except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "InputLoader.cuh"

namespace rtbf {

template <typename T_in, typename T_out>
InputLoader<T_in, T_out>::InputLoader(std::vector<size_t> dimensions,
                                      int deviceID, cudaStream_t cudaStream,
                                      std::string moniker,
                                      std::string loggerName) {
  // reset();  // Reset if previously initialized
  this->label = moniker;
  this->setLogger(loggerName);
  this->logdebug("Calling constructor.");

  // Populate parameters
  dims = dimensions;
  gpuID = deviceID;
  this->stream = cudaStream;
  this->out = std::make_shared<Tensor<T_out>>(
      dims, gpuID, this->label + "->out", this->logName);
}
template <typename T_in, typename T_out>
InputLoader<T_in, T_out>::~InputLoader() {
  this->logdebug("Calling destructor.");
}

template <typename T_in, typename T_out>
void InputLoader<T_in, T_out>::load(const T_out *h_raw, int pitchInSamps) {
  this->out->copyFromAsync(h_raw, this->stream, sizeof(T_out) * pitchInSamps);
}

template <typename T_in, typename T_out>
void InputLoader<T_in, T_out>::load(Tensor<T_out> *input) {
  this->out->copyFromAsync(input, this->stream);
}

template class InputLoader<short, short>;
template class InputLoader<float, float>;
template class InputLoader<cuda::std::complex<short>,
                           cuda::std::complex<short>>;
template class InputLoader<cuda::std::complex<float>,
                           cuda::std::complex<float>>;
}  // namespace rtbf
