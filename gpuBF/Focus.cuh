/**
 @file gpuBF/Focus.cuh
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2022-04-05

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef RTBF_FOCUS_CUH_
#define RTBF_FOCUS_CUH_

#include "Operator.cuh"
#include "interp1d.cuh"

namespace rtbf {

/** @brief Class to apply focusing delays to data. The output is always
demodulated focused channel data.

This class applies focusing delays and apodization profiles to the inputted
data. The data must be complex, but can be either radiofrequency or baseband.
The user provides delay and apodization profiles for the data. No aperture
synthesis is performed.
*/
template <typename T_in, typename T_out>
class Focus : public Operator<Tensor<T_in>, Tensor<T_out>> {
 private:
  // CUDA objects
  std::vector<cudaTextureObject_t> tex;  ///< Texture object
  cudaTextureDesc texDesc;               ///< Texture description
  cudaResourceDesc resDesc;              ///< Resource description for texture
  InterpMode imode;  ///< Interpolation mode (e.g., LINEAR or CUBIC)
  BFMode tmode;      ///< Beamforming mode
  BFMode rmode;      ///< Beamforming mode
  // Device version of variables
  Tensor<float> pxpos;  ///< Pixel positions
  Tensor<float> elpos;  ///< Element positions
  Tensor<float> txdat;  ///< Transmit data (foci or directions)
  Tensor<float> time0;  ///< Transmit time zeros
  float fs;             ///< Sampling frequency that data was acquired at
  float fc;             ///< Center frequency of original RF signal
  float txfn;           ///< Transmit f-number
  float rxfn;           ///< Receive f-number
  float c0;             ///< Speed of sound
  size_t npixels;       ///< Number of pixels
  size_t nxmits;        ///< Number of transmissions
  size_t nelems;        ///< Number of array elements
  int gpuID;            ///< Device for execution
  bool isBBIn;          ///< Boolean flag for whether input is baseband
  bool isBBOut;         ///< Boolean flag for whether output should be baseband

  // Compile-time type checking (both T_in and T_out must be real or complex)
  static_assert(
      is_complex<T_in>() == is_complex<T_out>(),
      "Focus performs real->real and complex->complex beamforming, but does "
      "not perform real->complex or complex->real beamforming.");

  /// @brief Initializer for the texture object.
  void initTextureObject();
  /// @brief Templatized kernel dispatcher.
  template <InterpMode imode>
  void focusHelper();
  /// @brief Input dimension checker. TODO: Need to restore functionality.
  void checkAllInputs();
  /// @brief Prepare the output Tensor based on beamforming modes.
  void makeOutputTensor();

 public:
  /** @brief Focus constructor.
  @param input Shared pointer to an input Tensor.
  @param pixelPos Pixel (x,y,z) coordinates as a [3, ...] Tensor.
  @param elemPos Element (x,y,z) coordinates as a [3, nelems] Tensor.
  @param txData A [2, nxmits] (angles) or [3, nxmits] (virtual source) Tensor.
  @param timeZeros The [nxmits,] Tensor to compensate for the time zero.
  @param fsample Sampling frequency [Hz].
  @param fcenter Center frequency [Hz].
  @param isInputBB Whether input is modulated or baseband.
  @param makeOutputBB Whether to produce modulated or baseband output.
  @param txfnumber The transmit f-number.
  @param rxfnumber The receive f-number.
  @param soundspeed Assumed reconstruction sound speed [m/s].
  @param txbfMode Transmit beamforming mode (SUM, IDENTITY, or BLKDIAG).
  @param rxbfMode Receive beamforming mode (SUM or IDENTITY).
  @param intrpMode Interpolation mode (NEAREST, LINEAR, CUBIC, or LANCZOS3).
  @param cudaStream CUDA stream for execution.
  @param moniker Name for FocusLUT Operator.
  @param loggerName The name of the spdlog logger to use.
 */
  Focus(std::shared_ptr<Tensor<T_in>> input, const Tensor<float> *pixelPos,
        const Tensor<float> *elemPos, const Tensor<float> *txData,
        const Tensor<float> *timeZeros, float fsample, float fcenter,
        bool isInputBB, bool makeOutputBB, float txfnumber = 2,
        float rxfnumber = 2, float soundspeed = 1540.f,
        BFMode txbfMode = BFMode::BLKDIAG, BFMode rxbfMode = BFMode::IDENTITY,
        InterpMode intrpMode = InterpMode::CUBIC, cudaStream_t cudaStream = 0,
        std ::string moniker = "Focus", std::string loggerName = "");
  /// @brief Destructor
  virtual ~Focus();
  /// @brief Apply focusing and apodization to data
  void focus();
};

namespace kernels::Focus {
/** @brief Core focusing kernel for block-diagonal image reconstruction.
 @param tex Texture object of the raw data.
 @param ppos Device pointer to the pixel coordinates.
 @param npx Total number of pixels in the image, excluding the transmit axis.
 @param tpos Device pointer to the transmit focus coordinates.
 @param ntx Total number of transmit events in the acquisition.
 @param rpos Device pointer to the receive element coordinates.
 @param nrx Total number of receive elements in the acquisition.
 @param fs Sampling frequency.
 @param fc Center frequency.
 @param c0inv Inverse of sound speed. (Inverse to avoid costly divisions.)
 @param nrxsum Number of receive elements to sum (nelems for SUM, else 1).
 @param txfn Transmit f-number.
 @param rxfn Receive f-number.
 @param t0 Device pointer to time zero vector.
 @param out Device pointer to output data.
 @param bb_input Whether input data is baseband or modulated.
 @param bb_output Whether output data should be baseband or modulated.
*/
template <typename T_out, InterpMode imode>
__global__ void focus_bd(cudaTextureObject_t tex, float3 *ppos, int npx,
                         float3 *tpos, int ntx, float3 *rpos, int nrx, float fs,
                         float fc, float c0inv, int nrxsum, float txfn,
                         float rxfn, float *t0, T_out *out, bool bb_input,
                         bool bb_output);

/** @brief Core focusing kernel for block-diagonal image reconstruction.
 @param tex Texture object of the raw data.
 @param ppos Device pointer to the pixel coordinates.
 @param npx Total number of pixels in the image, excluding the transmit axis.
 @param vpos Device pointer to the virtual focus coordinates.
 @param nvx Total number of transmit events in the acquisition.
 @param rpos Device pointer to the receive element coordinates.
 @param nrx Total number of receive elements in the acquisition.
 @param fs Sampling frequency.
 @param fc Center frequency.
 @param c0inv Inverse of sound speed. (Inverse to avoid costly divisions.)
 @param nvxsum Number of virtual sources to sum (nelems for SUM, else 1).
 @param nrxsum Number of receive elements to sum (nelems for SUM, else 1).
 @param txfn Transmit f-number.
 @param rxfn Receive f-number.
 @param t0 Device pointer to time zero vector.
 @param out Device pointer to output data.
 @param bb_input Whether input data is baseband or modulated.
 @param bb_output Whether output data should be baseband or modulated.
*/
template <typename T_out, InterpMode imode>
__global__ void focus_vs(cudaTextureObject_t tex, float3 *ppos, int npx,
                         float3 *vpos, int nvx, float3 *rpos, int nrx, float fs,
                         float fc, float c0inv, int nvxsum, int nrxsum,
                         float txfn, float rxfn, float *t0, T_out *out,
                         bool bb_input, bool bb_output);

/** @brief Core focusing kernel for block-diagonal image reconstruction.
 @param tex Texture object of the raw data.
 @param ppos Device pointer to the pixel coordinates.
 @param npx Total number of pixels in the image, excluding the transmit axis.
 @param tdir Device pointer to the plane wave steering angles (theta, phi).
 @param ntx Total number of transmit events in the acquisition.
 @param rpos Device pointer to the receive element coordinates.
 @param nrx Total number of receive elements in the acquisition.
 @param fs Sampling frequency.
 @param fc Center frequency.
 @param c0inv Inverse of sound speed. (Inverse to avoid costly divisions.)
 @param nvxsum Number of virtual sources to sum (nelems for SUM, else 1).
 @param nrxsum Number of receive elements to sum (nelems for SUM, else 1).
 @param txfn Transmit f-number.
 @param rxfn Receive f-number.
 @param t0 Device pointer to time zero vector.
 @param out Device pointer to output data.
 @param bb_input Whether input data is baseband or modulated.
 @param bb_output Whether output data should be baseband or modulated.
*/
template <typename T_out, InterpMode imode>
__global__ void focus_pw(cudaTextureObject_t tex, float3 *ppos, int npx,
                         float2 *tdir, int ntx, float3 *rpos, int nrx, float fs,
                         float fc, float c0inv, int ntxsum, int nrxsum,
                         float txfn, float rxfn, float *t0, T_out *out,
                         bool bb_input, bool bb_output);

}  // namespace kernels::Focus

// Add template type traits for SFINAE
template <typename>
struct is_Focus : std::false_type {};
template <typename T_in, typename T_out>
struct is_Focus<Focus<T_in, T_out>> : std::true_type {};

}  // namespace rtbf

#endif /* RTBF_FOCUS_CUH_ */
