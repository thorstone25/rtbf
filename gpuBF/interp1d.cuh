/**
 @file gpuBF/interp1d.cuh
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2021-03-08

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**	This file contains template device code that DataFormatter classes and
 * CUDA kernels can utilize for 1D interpolation.
 */

#ifndef INTERP1D_CUH_
#define INTERP1D_CUH_

#include "gpuBF.cuh"

namespace rtbf {
/// @brief Enum class to select interpolation mode
enum class InterpMode { NEAREST, LINEAR, CUBIC, LANCZOS3 };

namespace interp1d_functions {

/// @brief Samples tex at (xi, yi) and casts to float or complex<float>
template <typename T>
inline __device__ T getTypedTex2D(cudaTextureObject_t tex, float xi, float yi) {
  if constexpr (std::is_arithmetic<T>::value) {
    // If an ordinary type
    return tex2D<TexType<T>::type>(tex, xi, yi);
  } else {  // If a complex type
    return castToComplex(tex2D<TexType<T>::type>(tex, xi, yi));
  }
  return T(0);  // Suppress spurious warning. This line is never used.
}

/// @brief Device code template for nearest-neighbor interpolation
template <typename T>
__device__ T nearest(cudaTextureObject_t tex, float x, int idx) {
  return getTypedTex2D<T>(tex, roundf(x) + 0.5f, idx + 0.5f);
}

/// @brief Device code template for linear interpolation
template <typename T>
__device__ T linear(cudaTextureObject_t tex, float x, int idx) {
  return getTypedTex2D<T>(tex, x + 0.5f, idx + 0.5f);
}

/// @brief Device code template for cubic Hermite interpolation
template <typename T>
__device__ T cubic(cudaTextureObject_t tex, float x, int idx) {
  float xf;
  float u = modff(x, &xf);  // u is the fractional part, xf the integer part
  auto s0 = getTypedTex2D<T>(tex, xf - 1 + 0.5f, idx + 0.5f);
  auto s1 = getTypedTex2D<T>(tex, xf + 0 + 0.5f, idx + 0.5f);
  auto s2 = getTypedTex2D<T>(tex, xf + 1 + 0.5f, idx + 0.5f);
  auto s3 = getTypedTex2D<T>(tex, xf + 2 + 0.5f, idx + 0.5f);
  // Cubic Hermite interpolation (increased precision using fused multiply-adds)
  float a0 = 0 + u * (-1 + u * (+2 * u - 1));
  float a1 = 2 + u * (+0 + u * (-5 * u + 3));
  float a2 = 0 + u * (+1 + u * (+4 * u - 3));
  float a3 = 0 + u * (+0 + u * (-1 * u + 1));
  // // Cubic Hermite interpolation (naive, less precise implementation)
  // float a0 = -1 * u * u * u + 2 * u * u - 1 * u + 0;
  // float a1 = +3 * u * u * u - 5 * u * u + 0 * u + 2;
  // float a2 = -3 * u * u * u + 4 * u * u + 1 * u + 0;
  // float a3 = +1 * u * u * u - 1 * u * u + 0 * u + 0;
  return (s0 * a0 + s1 * a1 + s2 * a2 + s3 * a3) * 0.5f;
}

/// @brief Inline helper code for Lanczos 3-lobe interpolation
inline __host__ __device__ float lanczos_helper(float u, int a) {
  const float PI = atanf(1) * 4;
  return (u == 0.f) ? 1.f : sinpif(u) * sinpif(u / a) / (PI * PI * u * u);
}

/// @brief Device code template for Lanczos 3-lobe interpolation
template <typename T>
__device__ T lanczos3(cudaTextureObject_t tex, float x, int idx) {
  constexpr int a = 2;  // a=2 for 3-lobe Lanczos resampling
  float xf;
  float u = modff(x, &xf);  // u is the fractional part, xf the integer part
  auto s0 = getTypedTex2D<T>(tex, xf - 1 + 0.5f, idx + 0.5f);
  auto s1 = getTypedTex2D<T>(tex, xf + 0 + 0.5f, idx + 0.5f);
  auto s2 = getTypedTex2D<T>(tex, xf + 1 + 0.5f, idx + 0.5f);
  auto s3 = getTypedTex2D<T>(tex, xf + 2 + 0.5f, idx + 0.5f);
  float a0 = lanczos_helper(u + 1, a);
  float a1 = lanczos_helper(u + 0, a);
  float a2 = lanczos_helper(u - 1, a);
  float a3 = lanczos_helper(u - 2, a);
  return s0 * a0 + s1 * a1 + s2 * a2 + s3 * a3;
}

}  // namespace interp1d_functions
/// @brief Dispatcher for interp1d based on interpolation type
template <typename T, InterpMode imode>
inline __device__ auto interp1d(cudaTextureObject_t tex, float xi, float yi) {
  if constexpr (imode == InterpMode::NEAREST)
    return interp1d_functions::nearest<T>(tex, xi, yi);
  else if constexpr (imode == InterpMode::LINEAR)
    return interp1d_functions::linear<T>(tex, xi, yi);
  else if constexpr (imode == InterpMode::CUBIC)
    return interp1d_functions::cubic<T>(tex, xi, yi);
  else if constexpr (imode == InterpMode::LANCZOS3)
    return interp1d_functions::lanczos3<T>(tex, xi, yi);
  return T(0);  // Suppress spurious warning. This line should not be reached.
}
}  // namespace rtbf

#endif  // INTERP1D_CUH_