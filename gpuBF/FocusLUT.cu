/**
 @file gpuBF/FocusLUT.cu
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2022-04-22

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "FocusLUT.cuh"

namespace rtbf {
using IM = InterpMode;
template <typename T_in, typename T_out>
FocusLUT<T_in, T_out>::FocusLUT(
    std::shared_ptr<Tensor<T_in>> input, const Tensor<float> *txDelay,
    const Tensor<float> *rxDelay, const Tensor<float> *txApod,
    const Tensor<float> *rxApod, float fsample, float fcenter, float soundspeed,
    bool isInputBB, bool makeOutputBB, float pixelZSpacing, BFMode txbfMode,
    BFMode rxbfMode, InterpMode intrpMode, cudaStream_t cudaStream,
    std ::string moniker, std::string loggerName) {
  this->label = moniker;
  this->setLogger(loggerName);

  if constexpr (!is_complex<T_in>()) {
    this->loginfo("Using RF (real data) mode.");
    this->logdebug(
        "Ignoring isInputBB and makeOutputBB flags because baseband data "
        "cannot be represented using real values.");
  } else {
    this->loginfo("Using IQ (complex data) mode.");
    this->logdebug(
        "Input data given with fc={}Hz; Output data will have fc={}Hz.",
        isInputBB ? 0.f : fcenter, makeOutputBB ? 0.f : fcenter);
  }

  // Load input data
  this->in = input;  // Share ownership of input Tensor
  gpuID = this->in->getDeviceID();
  auto idims = this->in->getDimensions();
  if (gpuID < 0)
    this->template logerror<NotImplemented>("Cannot execute on CPU Tensors.");

  // Load scalars
  fs = fsample;
  fc = fcenter;
  c0 = soundspeed;
  dz = pixelZSpacing;
  tmode = txbfMode;
  rmode = rxbfMode;
  imode = intrpMode;
  this->stream = cudaStream;

  // Load beamforming parameters
  txdel = std::make_unique<Tensor<float>>(*txDelay, gpuID, "d_txdel",
                                          this->logName);
  rxdel = std::make_unique<Tensor<float>>(*rxDelay, gpuID, "d_rxdel",
                                          this->logName);
  if (txApod)
    txapo = std::make_unique<Tensor<float>>(*txApod, gpuID, "d_txapo",
                                            this->logName);
  if (rxApod)
    rxapo = std::make_unique<Tensor<float>>(*rxApod, gpuID, "d_rxapo",
                                            this->logName);
  pdims = rxdel->getDimensions();
  pdims.pop_back();

  auto tdims = txdel->getDimensions();
  auto rdims = rxdel->getDimensions();
  nxmits = !tdims.empty() ? tdims.back() : 1;
  nelems = !rdims.empty() ? rdims.back() : 1;
  checkAllInputs();     // Ensure that data are valid host pointers
  makeOutputTensor();   // Initialize arrays for output
  initTextureObject();  // Initialize texture object for input
}
template <typename T_in, typename T_out>
FocusLUT<T_in, T_out>::~FocusLUT() {
  if (gpuID >= 0) {
    CCE(cudaSetDevice(gpuID));
    for (auto &t : tex)
      if (t) CCE(cudaDestroyTextureObject(t));
  }
  this->loginfo("Destroyed {}.", this->label);
}

template <typename T_in, typename T_out>
void FocusLUT<T_in, T_out>::checkAllInputs() {}

template <typename T_in, typename T_out>
void FocusLUT<T_in, T_out>::makeOutputTensor() {
  // Define the output dimensions according to the beamforming configuration.
  // Use the receive delays as a guide for the eventual beamformed shape.
  // If rxdel->getDimensions() == {M, N, P, ..., nelems}, the output should have
  // dimensions == {M, N, P, ..., (1 or nxmits), (1 or nelems)}, depending on
  // whether the transmit and/or receive dimensions are summed.
  std::vector<size_t> odims = pdims;
  while (odims.size() < 2) odims.push_back(1);  // Make sure at least 2D result

  if (tmode == BFMode::IDENTITY) {
    this->loginfo("Preserving {} individual transmits (delay, no sum).",
                  nxmits);
    odims.push_back(nxmits);  // TX dimension has nxmits
  } else if (tmode == BFMode::SUM) {
    this->loginfo("Beamforming across {} transmits (delay and sum).", nxmits);
    odims.push_back(1);  // TX dimension is 1
  } else if (tmode == BFMode::BLKDIAG) {
    this->loginfo(
        "Transmits map to disjoint sets of pixels (ordinary beamforming).");
    if (odims.back() != nxmits)
      this->template logerror<std::invalid_argument>(
          "Last pixel dimension {} does not match the number of transmits "
          "{}. Cannot use BFMode::BLKDIAG.",
          odims.back(), nxmits);
  }
  if (rmode == BFMode::IDENTITY) {
    this->loginfo("Preserving {} receive elements (delay, no sum).", nelems);
    odims.push_back(nelems);
  } else if (rmode == BFMode::SUM) {
    this->loginfo("Beamforming across {} receive elements (delay and sum).",
                  nelems);
    odims.push_back(1);
  } else if (rmode == BFMode::BLKDIAG) {
    // TODO: Possible use-case: virtual receive array
    this->template logerror<NotImplemented>(
        "Not implemented. It is currently assumed that all receive elements "
        "are focused at all pixels.");
  }
  this->out = std::make_shared<Tensor<T_out>>(
      odims, gpuID, this->label + "->this->out", this->logName,
      false);  // gpuID >= 0);
}

template <typename T_in, typename T_out>
void FocusLUT<T_in, T_out>::initTextureObject() {
  if (gpuID < 0) return;
  // Set up textures to use the built-in bilinear interpolation hardware
  // Use texture objects (CC >= 3.0)
  // Texture description
  memset(&texDesc, 0, sizeof(texDesc));
  texDesc.addressMode[0] = cudaAddressModeBorder;
  texDesc.addressMode[1] = cudaAddressModeBorder;
  texDesc.filterMode = cudaFilterModeLinear;
  texDesc.normalizedCoords = 0;
  // Read mode depends on input and output types, determined at compile time.
  if constexpr (std::is_same_v<T_in, T_out>) {
    texDesc.readMode = cudaReadModeElementType;
  } else {
    static_assert(std::is_same_v<T_out, float> ||
                      std::is_same_v<T_out, cuda::std::complex<float>>,
                  "T_out must equal T_in or be floating point.");
    texDesc.readMode = cudaReadModeNormalizedFloat;
  }
  // Resource description
  std::vector<size_t> d = this->in->getDimensions();
  while (d.size() < 3) d.push_back(1);
  size_t framesize = d[0] * d[1] * d[2];
  size_t ntex = prod(d) / framesize;
  tex.resize(ntex, 0);

  CCE(cudaSetDevice(gpuID));
  for (size_t n = 0; n < ntex; n++) {
    if (tex[n] != 0) CCE(cudaDestroyTextureObject(tex[n]));
    memset(&resDesc, 0, sizeof(resDesc));
    resDesc.resType = cudaResourceTypePitch2D;
    resDesc.res.pitch2D.width = d[0];
    resDesc.res.pitch2D.height = d[1] * d[2];
    resDesc.res.pitch2D.desc =
        cudaCreateChannelDesc<typename TexType<T_in>::type>();
    resDesc.res.pitch2D.devPtr = this->in->data() + n * framesize;
    resDesc.res.pitch2D.pitchInBytes = this->in->getPitchInBytes();
    this->logdebug("Initializing CUDA texture object {} ({} x {}) at {}.", n,
                   d[0], d[1] * d[2], fmt::ptr(resDesc.res.pitch2D.devPtr));
    // Create texture object
    CCE(cudaCreateTextureObject(&tex[n], &resDesc, &texDesc, NULL));
  }
}

template <typename T_in, typename T_out>
void FocusLUT<T_in, T_out>::focus() {
  if (gpuID >= 0) {
    CCE(cudaSetDevice(gpuID));
    // Determine which CUDA kernel to call
    // Call the focus kernel. Explicitly specify the kernel template
    // parameters to achieve implicit template function instantiation. The
    // struct focusargs and function focusHelper are used purely to make the
    // following code more readable.
    if constexpr (is_complex<T_in>()) {
      this->logdebug("Executing IQ focus() on GPU{}", gpuID);
    } else {
      this->logdebug("Executing RF focus() on GPU{}", gpuID);
    }
    if (imode == IM::NEAREST) focusHelper<IM::NEAREST>();
    if (imode == IM::LINEAR) focusHelper<IM::LINEAR>();
    if (imode == IM::CUBIC) focusHelper<IM::CUBIC>();
    if (imode == IM::LANCZOS3) focusHelper<IM::LANCZOS3>();
  }
}

template <typename T_in, typename T_out>
template <InterpMode imode>
void FocusLUT<T_in, T_out>::focusHelper() {
  this->logdebug("Executing focus with isBBin={} and isBBout={}.", isBBIn,
                 isBBOut);
  // For the actual execution, use dim3 with the following dimensions:
  //    [pixels, nxmits_out, nelems_out].
  // This will be looped over prod(dims[3:]), i.e. all "frames".
  auto *tdel = txdel->data();
  auto *rdel = rxdel->data();
  auto *tapo = txapo ? txapo->data() : nullptr;
  auto *rapo = rxapo ? rxapo->data() : nullptr;
  int npixels = prod(pdims);
  auto npdims = pdims.size();
  auto odims = this->out->getDimensions();
  int nxmits_out = odims[npdims];
  int nelems_out = odims[npdims + 1];
  dim3 od3(npixels, nxmits_out, nelems_out);
  // Kernel launch parameters
  dim3 B(16, 4, 4);
  dim3 G((od3.x - 1) / B.x + 1, (od3.y - 1) / B.y + 1, (od3.z - 1) / B.z + 1);
  int nxmits_sum = nxmits / nxmits_out;  // Number of transmits to sum
  int nelems_sum = nelems / nelems_out;  // Number of elements to sum
  int framesize = od3.x * od3.y * od3.z;
  int nframes = prod(odims) / framesize;

  this->logdebug("Executing virtual source focusing.");
  this->out->resetToZeros();
  for (int f = 0; f < nframes; f++) {
    this->logdebug("Focusing frame {} of {}.", f + 1, nframes);
    T_out *d_out = this->out->data() + f * framesize;
    kernels::FocusLUT::focus_tables<T_out, imode><<<G, B, 0, this->stream>>>(
        tex[f], tdel, rdel, tapo, rapo, npixels, nxmits_out, nelems_out, fs, fc,
        pdims[0], dz, 1.f / c0, nxmits_sum, nelems_sum, d_out, isBBIn, isBBOut);
    getLastCudaError("Focusing kernel failed.\n");
  }
}

///////////////////////////////////////////////////////////////////////////
// Kernels
///////////////////////////////////////////////////////////////////////////
// Core kernel with template options for conditional execution. Templatizing
// the kernel allows the compiler to make branch-wise optimizations at
// compile-time. That is, the GPU will not actually go through the if/else
// branches for template parameters at runtime, and will instead execute the
// correct compiled version for each branch.
template <typename T_out, IM imode>
__global__ void kernels::FocusLUT::focus_tables(
    cudaTextureObject_t tex, float *tdel, float *rdel, float *tapo, float *rapo,
    int npx, int ntxo, int nrxo, float fs, float fc, int nrows, float dz,
    float c0inv, int ntxsum, int nrxsum, T_out *out, bool bb_input,
    bool bb_output) {
  // Determine information about the current thread
  int poidx = blockIdx.x * blockDim.x + threadIdx.x;  // pixel output index
  int toidx = blockIdx.y * blockDim.y + threadIdx.y;  // transmit output index
  int roidx = blockIdx.z * blockDim.z + threadIdx.z;  // receive output index
  // Only compute if valid output index
  if (poidx < npx && toidx < ntxo && roidx < nrxo) {
    int oidx = poidx + npx * (toidx + ntxo * roidx);  // output sample index
    T_out sum(0);                                     // initialize running sum
    // Loop through the transmit events to be summed
    for (int tidx = toidx * ntxsum; tidx < (toidx + 1) * ntxsum; tidx++) {
      float ta = tapo != nullptr ? tapo[poidx + npx * tidx] : 1.f;
      if (ta != 0.f) {  // If this transmit contributes to this pixel
        float td = tdel != nullptr ? tdel[poidx + npx * tidx] : 0.f;
        for (int ridx = roidx * nrxsum; ridx < (roidx + 1) * nrxsum; ridx++) {
          float ra = rapo != nullptr ? rapo[poidx + npx * ridx] : 1.f;
          if (ra != 0.f) {  // If this element contributes to this pixel
            float rd = rdel[poidx + npx * ridx];
            float tau = td + rd;      // Roundtrip time for current pixel
            float sample = tau * fs;  // in samples (interpolation coordinate)
            int line = tidx + ntxsum * ntxo * ridx;  // texture line index
            // Interpolate using imode (e.g., CUBIC, LINEAR)
            T_out data = interp1d<T_out, imode>(tex, sample, line);
            // If input is baseband data, undo reference phase rotation
            if constexpr (is_complex<T_out>()) {  // To avoid compiler errors
              if (bb_input) {
                float2 shift;  // Get the rotation complex phasor
                // Use the slower but more precise sincospif for large phases
                sincospif(-2.f * tau * fc, &shift.y, &shift.x);
                data *= castToComplex(shift);  // Apply phase shift
              }
            }
            sum += data;  // Accumulate data to synthesize the apertures
          }               // End if receive apodization
        }                 // End loop over receive elements
      }                   // End if transmit apodization
    }                     // End loop over transmits
    if constexpr (is_complex<T_out>()) {  // To avoid compiler errors
      if (bb_output) {  // Make the output baseband if requested
        // Get the current row
        int row = poidx % nrows;
        float2 demod;  // Get the demodulation complex phasor
        sincospif(-2.f * dz * row * c0inv * fc, &demod.y, &demod.x);
        sum *= castToComplex(demod);  // Apply demodulation
      }
    }
    // Store result
    out[oidx] = sum;
  }  // End if thread is valid
}

template class FocusLUT<short, float>;
template class FocusLUT<float, float>;
template class FocusLUT<cuda::std::complex<short>, cuda::std::complex<float>>;
template class FocusLUT<cuda::std::complex<float>, cuda::std::complex<float>>;

}  // namespace rtbf
