/**
 @file gpuBF/Tensor.cuh
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2022-03-29

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef RTBF_TENSOR_CUH_
#define RTBF_TENSOR_CUH_

#include "gpuBF.cuh"

namespace rtbf {

/** @brief Class for managing N-dimensional data arrays for GPU computing.

This class provides a simple implementation of an N-dimensional data array on
CPU or GPU devices, along with convenient utilities for transferring data across
devices and for logging. Internally, each Tensor maintains a pointer to host or
device allocated memory as well as its dimensions and is responsible for freeing
this memory upon destruction. Tensors can optionally use "pitched" memory
(padded along the innermost dimension) to improve data alignment on GPUs for
possibly faster access patterns. Tensors provide encapsulation so that any
allocated memory is automatically destroyed when the Tensor goes out of scope,
eliminating the need for "free" or "cudaFree" calls from user code.
*/
template <typename T>
class Tensor {
 protected:
  // Required data pointers and structures
  T *ptr = nullptr;          ///< Internal pointer to raw data
  std::vector<size_t> dims;  ///< Dimensions of data
  int pitch;                 ///< Pitch of array in elements.

  // Device information and CUDA objects
  int gpuID = -1;     ///< Index of the GPU that the data array will reside on.
  std::string label;  ///< Optional string label for data array.
  bool pitched;       ///< Boolean to mark whether memory should be pitched.
  std::string logName;  ///< Name of the spdlog logger to use for this Tensor.
  std::shared_ptr<spdlog::logger> slog;  ///< Shared pointer to spdlog logger.

  // Utility functions
  /// @brief Allocate memory
  void allocate();
  /// @brief Error logging function that prepends Tensor information
  template <class except = std::runtime_error, typename FormatString,
            typename... Args>
  inline void logerror(const FormatString &fmt, Args &&...args) {
    auto str = "(" + label + ")" + ": " + fmt::format(fmt, args...);
    slog->error(str);
    throw except(str);
  }
  /// @brief Warning logging function that prepends Tensor information
  template <typename FormatString, typename... Args>
  inline void logwarn(const FormatString &fmt, Args &&...args) {
    slog->warn("(" + label + ")" + ": " + fmt::format(fmt, args...));
  }
  /// @brief Information logging function that prepends Tensor information
  template <typename FormatString, typename... Args>
  inline void loginfo(const FormatString &fmt, Args &&...args) {
    slog->info("(" + label + ")" + ": " + fmt::format(fmt, args...));
  }
  /// @brief Debug logging function that prepends Tensor information
  template <typename FormatString, typename... Args>
  inline void logdebug(const FormatString &fmt, Args &&...args) {
    slog->debug("(" + label + ")" + ": " + fmt::format(fmt, args...));
  }

 public:
  /// @brief Default constructor (creates empty Tensor)
  Tensor();
  /// @brief Constructor
  Tensor(std::vector<size_t> dimensions, int deviceID = 0,
         std::string dataLabel = "", std::string loggerName = "",
         bool usePitched = false);
  /// @brief No default copy constructor allowed. Must use explicit (deep) copy.
  Tensor(const Tensor<T> &A, std::optional<int> deviceID = std::nullopt,
         std::optional<std::string> copyLabel = std::nullopt,
         std::optional<std::string> loggerName = std::nullopt,
         std::optional<bool> usePitched = std::nullopt);
  /// @brief Move-assignment operator
  Tensor<T> &operator=(Tensor<T> &&A);
  /// @brief Destructor
  virtual ~Tensor();
  /// @brief Dummy value to help with type inference
  T val = T{0};

  /// @brief Return true if dimensions are not empty and are non-zero.
  bool empty() const { return dims.empty() || prod(dims) == 0; }
  /// @brief Return true if the Tensor points to valid data.
  bool valid() const { return ptr != nullptr; }
  /// @brief Returns whether the Tensor is pitched
  bool isPitched() const { return pitched; }
  /// @brief Returns whether the Tensor has padding for pitch
  bool isPadded() const { return dims[0] != pitch; }
  /// @brief Throw error if the Tensor is not ready for use (empty or invalid).
  void checkInit();

  /// @brief Indexing `operator[]` overload
  T operator[](size_t i) const { return ptr[i]; }
  /// @brief Indexing `operator[]` overload
  T &operator[](size_t i) { return ptr[i]; }
  /// @brief Returns the device pointer to the data
  virtual T *data() const { return ptr; }
  /// @brief Indicator if this Tensor is using the GPU
  bool usingGPU() const { return gpuID >= 0; }
  /// @brief Returns the GPU index for this object
  int getDeviceID() const { return gpuID; }
  /// @brief Returns the pitch in elements of T
  size_t getPitch() const { return pitch; }
  /// @brief Returns the pitch in bytes
  size_t getPitchInBytes() const { return sizeof(T) * pitch; }
  /// @brief Returns the total size of the array in bytes
  size_t getSizeInBytes() const { return pprod(dims) * sizeof(T); }
  /// @brief Returns the total size of the array in elements
  size_t getNumberOfValidElements() const { return prod(dims); }
  /// @brief Returns the total size of the actual array in elements
  size_t getNumberOfElements() const { return pprod(dims); }
  /// @brief Returns the tensor rank of this Tensor
  size_t getRank() const { return dims.size(); }
  /// @brief Returns the dimensions of the data
  std::vector<size_t> getDimensions() const { return dims; }
  /// @brief Product of the dimensions, but using the pitch for dimension 0.
  size_t pprod(std::vector<size_t> dims) const {
    return prod(dims) / dims[0] * pitch;
  }
  /// @brief Return a printable string including pitch
  std::string vecStringPitched() const;
  /// @brief Returns the label for this object
  std::string getLabel() const { return label; }
  /// @brief Returns the logger name for this object
  std::string getLogger() const { return logName; }
  /// @brief Get a string containing the device information
  inline std::string devstr() const {
    return gpuID >= 0 ? fmt::format("GPU{}", gpuID) : "CPU";
  }

  /// @brief Reset all values to zeros
  void resetToZeros();
  /// @brief Reset all values to zeros asynchronously
  void resetToZerosAsync(cudaStream_t stream);
  /// @brief Reshape the array
  void reshape(const std::vector<size_t> &newDims);

  /// @brief Sets the label for this object
  void setLabel(std::string str) { label = str; }
  /// @brief Set the logger to write to some existing logger
  void setLogger(std::string loggerName);

  // Copy to and from raw data pointers
  /// @brief Copy from pointer
  void copyFrom(const T *src, size_t h_pitchInBytes = 0);
  /// @brief Copy data from pointer asynchronously
  void copyFromAsync(const T *src, cudaStream_t stream,
                     size_t h_pitchInBytes = 0);
  /// @brief Copy to pointer
  void copyTo(T *dst, size_t h_pitchInBytes = 0);
  /// @brief Copy data to pointer asynchronously
  void copyToAsync(T *dst, cudaStream_t stream, size_t h_pitchInBytes = 0);

  // Copy to and from Tensors
  /// @brief Copy from Tensor
  void copyFrom(Tensor<T> *src);
  /// @brief Copy data from Tensor asynchronously
  void copyFromAsync(Tensor<T> *src, cudaStream_t stream);
  /// @brief Copy to Tensor
  void copyTo(Tensor<T> *dst);
  /// @brief Copy data to Tensor asynchronously
  void copyToAsync(Tensor<T> *dst, cudaStream_t stream);

  // /// @brief Make a deep copy of the current Tensor on a specified device
  // Tensor<T> makeCopy(int deviceID, std::string copyLabel = "");
};

/** @brief Class to provide a Tensor-like view of existing data.

Alternatively, this class can provide a Tensor<T>-like "view" of an existing raw
data pointer without actually taking ownership, which can be useful for
applications where the raw data is supplied in a raw read-only buffer, such as
MATLAB inputs. TensorView objects can be treated as ordinary Tensors, and differ
only in the (lack of) allocation and destruction of memory. In this use case, it
is the user's responsibility to manage the lifetime of the underlying memory.
*/

template <typename T>
class TensorView : public Tensor<T> {
 public:
  /// @brief Default constructor does nothing.
  TensorView() {}

  /// @brief Constructor
  TensorView(std::vector<size_t> dimensions, T *data, int deviceID = 0,
             std::string dataLabel = "", std::string loggerName = "") {
    // Parse inputs
    this->setLogger(loggerName);  // Set the logger
    this->ptr = data;             // Get the data pointer
    this->dims = dimensions;      // TensorView dimensions
    this->gpuID = deviceID;       // Device to use
    this->label = dataLabel;      // Identifier for current TensorView
    this->pitch = this->dims[0];  // Assume pitch == dims[0]
    this->pitched = false;        // Flag to use pitched memory
    validatePointer();            // Make sure pointer is usable as specified
    this->loginfo("Creating a TensorView of size {} on {} at pointer {}.",
                  vecString(this->dims), this->devstr(), fmt::ptr(this->ptr));
  }

  /// @brief Constructor (with pitch)
  TensorView(std::vector<size_t> dimensions, size_t dataPitch, T *data,
             int deviceID = 0, std::string dataLabel = "",
             std::string loggerName = "") {
    // Parse inputs
    this->setLogger(loggerName);  // Set the logger
    this->ptr = data;             // Get the data pointer
    this->dims = dimensions;      // TensorView dimensions
    this->gpuID = deviceID;       // Device to use
    this->label = dataLabel;      // Identifier for current TensorView
    this->pitch = dataPitch;      // Pitch of the input data
    this->pitched = true;         // Flag to use pitched memory
    validatePointer();            // Make sure pointer is usable as specified
    this->loginfo("Creating a TensorView of size {} on {} at pointer {}.",
                  this->vecStringPitched(), this->devstr(),
                  fmt::ptr(this->ptr));
  }

  /// @brief Constructor
  TensorView(Tensor<T> &input, std::string dataLabel = "",
             std::string loggerName = "") {
    // Parse inputs
    this->setLogger(loggerName);         // Set the logger
    this->ptr = input.data();            // Get the data pointer
    this->dims = input.getDimensions();  // TensorView dimensions
    this->gpuID = input.getDeviceID();   // Device to use
    this->label = input.getLabel();      // Identifier for current TensorView
    this->pitch = input.getPitch();      // Pitch of the input data
    this->pitched = input.isPitched();   // Flag to use pitched memory
    this->loginfo("Creating a TensorView of size {} on {} at pointer {}.",
                  vecString(this->dims), this->devstr(), fmt::ptr(this->ptr));
  }

  /// @brief Do error checking to ensure the pointer is valid and accessible
  void validatePointer() {
    // If using the GPU, we must ensure that the ptr is safe to use
    cudaPointerAttributes attr;
    CCE(cudaPointerGetAttributes(&attr, this->ptr));
    if (this->gpuID >= 0) {  // On the GPU
      if (attr.devicePointer != this->ptr)
        this->logerror("{} is not valid pointer on {}.", fmt::ptr(this->ptr),
                       this->devstr());
    } else {  // On the CPU
      if (attr.devicePointer != nullptr)
        this->logerror("{} is not a valid pointer on {}.", fmt::ptr(this->ptr),
                       this->devstr());
    }
  }

  /// @brief Destructor simply sets ptr to nullptr.
  /// TensorViews have no ownership of the underlying data, and so should leave
  /// it untouched upon destruction. Setting ptr to nullptr ensures that the
  /// base class destructor ~Tensor() will not free the memory at ptr.
  virtual ~TensorView() {
    this->ptr = nullptr;
    this->loginfo("Destroyed TensorView.");
  }
};

}  // namespace rtbf

#endif /* RTBF_TENSOR_CUH_ */
