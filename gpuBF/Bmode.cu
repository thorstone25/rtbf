/**
 @file gpuBF/Bmode.cu
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2022-03-29

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "Bmode.cuh"

namespace rtbf {

template <typename T_in, typename T_out>
Bmode<T_in, T_out>::Bmode(std::shared_ptr<Tensor<T_in>> input,
                          std::optional<int> compoundAxis,
                          cudaStream_t cudaStream, std::string moniker,
                          std::string loggerName) {
  this->label = moniker;
  this->setLogger(loggerName);

  // Parse inputs
  this->in = input;           // Share ownership of input Tensor
  axis = compoundAxis;        // Dimension to compound (std::nullopt if none)
  this->stream = cudaStream;  // Set asynchronous stream
  gpuID = this->in->getDeviceID();  // Execute on input's device

  // Get input dimensions
  idims = this->in->getDimensions();  // Get the input data dimensions
  // If axis < 0, no incoherent compounding is requested. Create a dummy
  // singleton dimension and "sum" over it.
  if (axis == std::nullopt) {  // If not requesting incoherent compounding
    if (idims.back() != 1) idims.push_back(1);
    axis = idims.size() - 1;
  }
  // Check that the summing dimension is valid
  if (*axis >= idims.size()) {
    this->template logerror<std::out_of_range>(
        "Channel dimension {:d} is invalid. Must be in range [0,{:d}).", *axis,
        idims.size());
  }
  // TODO: Add functionality to sum dimensions that are not the last dimension
  if (*axis != idims.size() - 1)
    this->template logerror<std::invalid_argument>(
        "Currently, the compounding dimension must be the last dimension."
        "Compounding other dimensions will be added as a feature later.");

  // Initialize an output array
  this->loginfo("Creating B-mode envelope data.");
  odims = idims;
  if (axis == std::nullopt && idims.back() != 1) odims.pop_back();
  this->out = std::make_shared<Tensor<T_out>>(
      odims, gpuID, this->label + "->out", this->logName);
}

template <typename T_in, typename T_out>
Bmode<T_in, T_out>::~Bmode() {}  // Nothing to do

template <typename T_in, typename T_out>
void Bmode<T_in, T_out>::execute_core(int mode, float arg) {
  // Get pointers to the data
  T_in *d_in = this->in->data();
  T_out *d_out = this->out->data();
  // If using the GPU, launch a CUDA kernel
  if (gpuID >= 0) {
    CCE(cudaSetDevice(gpuID));
    // Convert the n-dimensional size_t vector into a 3D vector
    dim3 id3 = dimsForCUDAKernels(idims);
    dim3 od3 = dimsForCUDAKernels(odims);
    dim3 B(256, 1, 1);
    dim3 G((od3.x - 1) / B.x + 1, (od3.y - 1) / B.y + 1, (od3.z - 1) / B.z + 1);
    size_t ipitch = this->in->getPitch();
    size_t opitch = this->out->getPitch();
    // Execute
    if (mode == 0) {
      kernels::Bmode::envDetect<<<G, B, 0, this->stream>>>(d_in, id3, ipitch,
                                                           d_out, od3, opitch);
    } else if (mode == 1) {
      kernels::Bmode::envDetectLogComp<<<G, B, 0, this->stream>>>(
          d_in, id3, ipitch, d_out, od3, opitch);
    } else if (mode == 2) {
      kernels::Bmode::envDetectPowComp<<<G, B, 0, this->stream>>>(
          d_in, id3, ipitch, d_out, od3, opitch, arg);
    }
  } else {
    if (mode == 0) {
      kernels::Bmode::h_envDetect(d_in, idims, d_out, odims);
    } else if (mode == 1) {
      kernels::Bmode::h_envDetect(d_in, idims, d_out, odims);
      kernels::Bmode::h_logCompress(d_out, odims);
    } else if (mode == 2) {
      kernels::Bmode::h_envDetect(d_in, idims, d_out, odims);
      kernels::Bmode::h_powCompress(d_out, odims, arg);
    }
  }
}

template <typename T_in, typename T_out>
void Bmode<T_in, T_out>::getEnvelope() {
  execute_core(0);
}

template <typename T_in, typename T_out>
void Bmode<T_in, T_out>::getEnvelopeLogCompress() {
  execute_core(1);
}

template <typename T_in, typename T_out>
void Bmode<T_in, T_out>::getEnvelopePowCompress(float gamma) {
  execute_core(2, gamma);
}

///////////////////////////////////////////////////////////////////////////
// Kernels
///////////////////////////////////////////////////////////////////////////

template <typename T_in, typename T_out>
void kernels::Bmode::h_envDetect(T_in *idata, std::vector<size_t> idims,
                                 T_out *odata, std::vector<size_t> odims) {
  // To handle n-d data, we reshape the data to be 2D
  std::vector<size_t> idim2;
  idim2.push_back(
      prod(std::vector<size_t>(idims.begin(), std::prev(idims.end()))));
  idim2.push_back(idims.back());
  for (auto y = 0; y < idim2[1]; y++) {
    for (auto x = 0; x < idim2[0]; x++) {
      odata[x] += absf(idata[x + idim2[0] * y]);
    }
  }
}

template <typename T_out>
void kernels::Bmode::h_logCompress(T_out *odata, std::vector<size_t> odims) {
  for (auto i = 0; i < prod(odims); i++) odata[i] = 20 * log10f(odata[i]);
}

template <typename T_out>
void kernels::Bmode::h_powCompress(T_out *odata, std::vector<size_t> odims,
                                   float gamma) {
  for (auto i = 0; i < prod(odims); i++) odata[i] = powf(odata[i], gamma);
}

template <typename T_in, typename T_out>
__global__ void kernels::Bmode::envDetect(T_in *idata, dim3 idims,
                                          size_t ipitch, T_out *odata,
                                          dim3 odims, size_t opitch) {
  int x = threadIdx.x + blockIdx.x * blockDim.x;
  int y = threadIdx.y + blockIdx.y * blockDim.y;
  if (x < odims.x && y < odims.y) {
    // Compute output index
    int oidx = x + opitch * y;
    // Initialize running sum
    T_out sum = (T_out)0.f;
    // Loop through any images to compound
    for (int z = 0; z < idims.z; z++)
      sum += absf(idata[x + ipitch * (y + idims.y * z)]);
    // Write to output
    odata[oidx] = sum;
  }
}

template <typename T_in, typename T_out>
__global__ void kernels::Bmode::envDetectLogComp(T_in *idata, dim3 idims,
                                                 size_t ipitch, T_out *odata,
                                                 dim3 odims, size_t opitch) {
  int x = threadIdx.x + blockIdx.x * blockDim.x;
  int y = threadIdx.y + blockIdx.y * blockDim.y;
  if (x < odims.x && y < odims.y) {
    // Compute output index
    int oidx = x + opitch * y;
    // Initialize running sum
    T_out sum = (T_out)0.f;
    // Loop through any images to compound
    for (int z = 0; z < idims.z; z++)
      sum += absf(idata[x + ipitch * (y + idims.y * z)]);
    // Write to output
    odata[oidx] = 20.f * log10f(sum);
  }
}

template <typename T_in, typename T_out>
__global__ void kernels::Bmode::envDetectPowComp(T_in *idata, dim3 idims,
                                                 size_t ipitch, T_out *odata,
                                                 dim3 odims, size_t opitch,
                                                 float gamma) {
  int x = threadIdx.x + blockIdx.x * blockDim.x;
  int y = threadIdx.y + blockIdx.y * blockDim.y;
  if (x < odims.x && y < odims.y) {
    // Compute output index
    int oidx = x + opitch * y;
    // Initialize running sum
    T_out sum = (T_out)0.f;
    // Loop through any images to compound
    for (int z = 0; z < idims.z; z++)
      sum += absf(idata[x + ipitch * (y + idims.y * z)]);
    // Write to output
    odata[oidx] = powf(sum, gamma);
  }
}

// Explicit template specialization instantiation
template class Bmode<cuda::std::complex<short>, float>;
template class Bmode<cuda::std::complex<float>, float>;

}  // namespace rtbf
