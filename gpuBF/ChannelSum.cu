/**
 @file gpuBF/ChannelSum.cu
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2022-03-31

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "ChannelSum.cuh"

namespace rtbf {
namespace kernels = ChannelSumKernels;
template <typename T_in, typename T_out>
ChannelSum<T_in, T_out>::ChannelSum(std::shared_ptr<Tensor<T_in>> input,
                                    int axisToSum, int nOutputChannels,
                                    cudaStream_t cudaStream,
                                    std::string moniker,
                                    std::string loggerName) {
  this->label = moniker;
  this->setLogger(loggerName);
  this->logdebug("Initializing {}.", this->label);

  // Read inputs
  this->in = input;              // Share ownership of input Tensor
  axis = axisToSum;              // Axis to sum over
  nchans_out = nOutputChannels;  // Number of channels to output
  this->stream = cudaStream;     // Set asynchronous stream
  gpuID =
      this->in->getDeviceID();  // Execution will take place on input's device
  idims = this->in->getDimensions();  // Get dimensions of input

  // Allow Python-style negative indexing
  if (axis < 0) axis += idims.size();
  if (axis < 0 && axis >= idims.size())
    this->template logerror<std::out_of_range>(
        "Invalid axis {} to sum over. Must be in range [-{}, {})", axis,
        idims.size(), idims.size());
  if (nchans_out > idims[axis]) {
    this->loginfo(
        "Requesting more output channels ({}) than available (dims[{}]={}). "
        "Setting to maximum.",
        nchans_out, axis, idims[axis]);
    nchans_out = idims[axis];
  }
  odims = idims;
  odims[axis] = nchans_out;
  this->loginfo(
      "Initializing sum over axis {} (input dims: {} --> output dims: {}).",
      axis, vecString(idims), vecString(odims));

  // If nchans_out == idims[axis] and T_in == T_out, this is simply an identity
  // operation. We can simply pass the input Tensor as the output without
  // allocating memory or computing anything. Use constexpr to avoid compiler
  // errors about type mismatches between T_in and T_out.
  if constexpr (std::is_same_v<T_in, T_out>) {
    isZeroCopy = odims == idims;
    if (isZeroCopy) {
      this->loginfo(
          "Requested number of output channels {} equals the number of input "
          "channels {}. Using zero-copy mode.",
          nchans_out, idims[axis]);
      this->out = this->in;
      return;
    }
  }
  // If not in "zero-copy" mode, make an output array
  this->out = std::make_shared<Tensor<T_out>>(
      odims, gpuID, this->label + "->out", this->logName);
}

template <typename T_in, typename T_out>
ChannelSum<T_in, T_out>::~ChannelSum() {
  // Nothing to do in destructor. Everything will naturally fall out of scope.
  this->loginfo("Destroyed {}.", this->label);
}

template <typename T_in, typename T_out>
void ChannelSum<T_in, T_out>::sumChannels(bool normalize) {
  this->logdebug("Executing channel sum.");
  if (isZeroCopy) return;  // Do nothing if in identity (zero-copy) mode.
  // In the special case that a = 0, we must take special care to work around
  // the pitch of the Tensor. Dispatch to a separate function.
  if (axis == 0) {
    sumChannelsDim0(normalize);
    return;
  }
  // To allow summing over an arbitrary axis, we reshape the n-dimensional
  // Tensor into a 4D array. We use the following canonical dimensions:
  //   dim0: pitch dimension
  //   dim1: all dimensions in interval [1, axis-1)
  //   dim2: only dimension axis
  //   dim3: all dimensions in interval [axis+1, end)
  // dims 1 and 3 and singleton dimensions if the interval is empty.
  std::vector<size_t> idpad = {idims[0], 1, 1, 1};
  std::vector<size_t> odpad = {odims[0], 1, 1, 1};
  for (int a = 1; a < axis; a++) {
    idpad[1] *= idims[a];
    odpad[1] *= odims[a];
  }
  idpad[2] = idims[axis];
  odpad[2] = odims[axis];
  for (int a = axis + 1; a < idims.size(); a++) {
    idpad[3] *= idims[a];
    odpad[3] *= odims[a];
  }
  // If everything is right, idpad should match odpad in all except dim 2.
  if (idpad[0] != odpad[0] || idpad[1] != odpad[1] || idpad[3] != odpad[3])
    this->template logerror<std::logic_error>("Something fundamental is wrong here.");
  T_in *d_in = this->in->data();
  T_out *d_out = this->out->data();
  int ipitch = static_cast<int>(this->in->getPitch());
  int opitch = static_cast<int>(this->out->getPitch());
  int dsfactor = idpad[2] / odpad[2];

  // Execute the summation kernel
  if (gpuID >= 0) {  // Execute on the GPU
      CCE(cudaSetDevice(gpuID));
      dim3 id3(idpad[0], idpad[1], idpad[2]);
      dim3 od3(odpad[0], odpad[1], odpad[2]);
      int nframes = idpad[3];
      // Set up CUDA kernel launch parameters
      dim3 B(256, 1, 1);
      dim3 G((od3.x - 1) / B.x + 1, (od3.y - 1) / B.y + 1,
             (od3.z - 1) / B.z + 1);
      // Execute
      if (normalize) {
        kernels::sumChannels<true><<<G, B, 0, this->stream>>>(
            d_in, id3, ipitch, d_out, od3, opitch, dsfactor, nframes);
      } else {
        kernels::sumChannels<false><<<G, B, 0, this->stream>>>(
            d_in, id3, ipitch, d_out, od3, opitch, dsfactor, nframes);
      }
  } else {  // Execute on the CPU
      kernels::h_sumChannels(d_in, idpad, ipitch, d_out, odpad, opitch,
                             dsfactor, normalize);
  }
}

template <typename T_in, typename T_out>
void ChannelSum<T_in, T_out>::sumChannelsDim0(bool normalize) {
  // Summing over the first axis (dims[0]) is much easier.
  // Use a 3D representation of the data.
  std::vector<size_t> idpad = {idims[0], 1, 1};
  std::vector<size_t> odpad = {odims[0], 1, 1};
  for (int a = 1; a < 2 && a < idims.size(); a++) {
    idpad[1] *= idims[a];
    odpad[1] *= odims[a];
  }
  for (int a = 2; a < idims.size(); a++) {
    idpad[2] *= idims[a];
    odpad[2] *= odims[a];
  }

  T_in *d_in = this->in->data();
  T_out *d_out = this->out->data();
  int ipitch = static_cast<int>(this->in->getPitch());
  int opitch = static_cast<int>(this->out->getPitch());
  int dsfactor = idpad[0] / odpad[0];

  // Execute the summation kernel
  if (gpuID >= 0) {  // Execute on the GPU
    CCE(cudaSetDevice(gpuID));
    dim3 id3(idpad[0], idpad[1], idpad[2]);
    dim3 od3(odpad[0], odpad[1], odpad[2]);
    // Set up CUDA kernel launch parameters
    dim3 B(256, 1, 1);
    dim3 G((od3.x - 1) / B.x + 1, (od3.y - 1) / B.y + 1, (od3.z - 1) / B.z + 1);
    // Execute
    if (normalize) {
      kernels::sumChannelsDim0<true><<<G, B, 0, this->stream>>>(
          d_in, id3, ipitch, d_out, od3, opitch, dsfactor);
    } else {
      kernels::sumChannelsDim0<false><<<G, B, 0, this->stream>>>(
          d_in, id3, ipitch, d_out, od3, opitch, dsfactor);
    }
  } else {  // Execute on the CPU
    kernels::h_sumChannelsDim0(d_in, idpad, ipitch, d_out, odpad, opitch,
                               dsfactor, normalize);
  }
}

template <typename T_in, typename T_out>
void kernels::h_sumChannels(T_in *idata, std::vector<size_t> idims,
                            size_t ipitch, T_out *odata,
                            std::vector<size_t> odims, size_t opitch, int ds,
                            bool normalize) {
  // Always sum over z
  for (size_t f = 0; f < odims[3]; f++) {
    for (size_t y = 0; y < odims[1]; y++) {
      for (size_t x = 0; x < odims[0]; x++) {
        for (size_t z = 0; z < odims[2]; z++) {
          size_t oidx = x + opitch * (y + odims[1] * z);
          T_out sum(0);
          int stop_elem = (z == odims[2] - 1) ? idims[2] : (z + 1) * ds;
          for (int elem = z * ds; elem < stop_elem; elem++) {
            if (elem < idims[2]) {
              sum += idata[x + ipitch * (y + idims[1] * elem)];
            }
          }
          if (normalize) sum *= 1.f / (stop_elem - z * ds);  // Normalize
          odata[oidx] = sum;
        }
      }
    }
    // Advance pointers by one frame
    idata += ipitch * idims[1] * idims[2];
    odata += opitch * odims[1] * odims[2];
  }
}

template <bool normalize, typename T_in, typename T_out>
__global__ void kernels::sumChannels(T_in *idata, dim3 idims, int ipitch,
                                     T_out *odata, dim3 odims, int opitch,
                                     int ds, int nframes) {
  // Always sum over z
  int x = threadIdx.x + blockIdx.x * blockDim.x;
  int y = threadIdx.y + blockIdx.y * blockDim.y;
  int z = threadIdx.z + blockIdx.z * blockDim.z;
  if (x < odims.x && y < odims.y && z < odims.z) {
    // Compute output index
    int oidx = x + opitch * (y + odims.y * z);
    for (int f = 0; f < nframes; f++) {
      T_out sum(0);
      int stop_elem = (z == odims.z - 1) ? idims.z : (z + 1) * ds;
      for (int elem = z * ds; elem < stop_elem; elem++) {
        if (elem < idims.z) {
          sum += idata[x + ipitch * (y + idims.y * elem)];
        }
      }
      if (normalize) sum *= 1.f / (stop_elem - z * ds);  // Normalize
      odata[oidx] = sum;
      // Advance pointers by one frame
      idata += ipitch * idims.y * idims.z;
      odata += opitch * odims.y * odims.z;
    }
  }
}

template <typename T_in, typename T_out>
void kernels::h_sumChannelsDim0(T_in *idata, std::vector<size_t> idims,
                                size_t ipitch, T_out *odata,
                                std::vector<size_t> odims, size_t opitch,
                                int ds, bool normalize) {
  // Always sum over z
  for (size_t z = 0; z < odims[2]; z++) {
    for (size_t y = 0; y < odims[1]; y++) {
      for (size_t x = 0; x < odims[0]; x++) {
        size_t oidx = x + opitch * (y + odims[1] * z);
        T_out sum(0);
        int stop_elem = (x == odims[0] - 1) ? idims[0] : (x + 1) * ds;
        for (int elem = x * ds; elem < stop_elem; elem++) {
          if (elem < idims[0]) {
            sum += idata[elem + ipitch * (y + idims[1] * z)];
          }
        }
        if (normalize) sum *= 1.f / (stop_elem - x * ds);  // Normalize
        odata[oidx] = sum;
      }
    }
  }
}

template <bool normalize, typename T_in, typename T_out>
__global__ void kernels::sumChannelsDim0(T_in *idata, dim3 idims, int ipitch,
                                         T_out *odata, dim3 odims, int opitch,
                                         int ds) {
  // Always sum over z
  int x = threadIdx.x + blockIdx.x * blockDim.x;
  int y = threadIdx.y + blockIdx.y * blockDim.y;
  int z = threadIdx.z + blockIdx.z * blockDim.z;
  if (x < odims.x && y < odims.y && z < odims.z) {
    // Compute output index
    int oidx = x + opitch * (y + odims.y * z);
    T_out sum(0);
    int stop_elem = (x == odims.x - 1) ? idims.x : (x + 1) * ds;
    for (int elem = x * ds; elem < stop_elem; elem++) {
      if (elem < idims.x) {
        sum += idata[elem + ipitch * (y + idims.y * z)];
      }
    }
    if (normalize) sum *= 1.f / (stop_elem - x * ds);  // Normalize
    odata[oidx] = sum;
  }
}

// Explicit template specialization instantiation for faster compiling
template class ChannelSum<cuda::std::complex<short>, cuda::std::complex<short>>;
template class ChannelSum<cuda::std::complex<short>, cuda::std::complex<int>>;
template class ChannelSum<cuda::std::complex<short>, cuda::std::complex<float>>;
template class ChannelSum<cuda::std::complex<int>, cuda::std::complex<int>>;
template class ChannelSum<cuda::std::complex<int>, cuda::std::complex<float>>;
template class ChannelSum<cuda::std::complex<float>, cuda::std::complex<float>>;
template class ChannelSum<short, short>;
template class ChannelSum<short, int>;
template class ChannelSum<short, float>;
template class ChannelSum<int, int>;
template class ChannelSum<int, float>;
template class ChannelSum<float, float>;

}  // namespace rtbf
