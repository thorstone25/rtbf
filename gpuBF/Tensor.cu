/**
 @file gpuBF/Tensor.cu
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2022-03-29

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "Tensor.cuh"

namespace rtbf {

/// The default constructor creates an uninitialized Tensor pointing to
/// `nullptr` with no dimensions. This Tensor is not valid, and must be
/// destroyed and re-constructed to be used properly. The default constructor
/// exists solely so that Tensors can be defined in an uninitialized state.
template <typename T>
Tensor<T>::Tensor() {}

/// This constructor will create a new Tensor with the specified dimensions on
/// the specified device. This constructor will probably be the most used among
/// the available constructors.
template <typename T>
Tensor<T>::Tensor(std::vector<size_t> dimensions, int deviceID,
                  std::string dataLabel, std::string loggerName,
                  bool usePitched) {
  // Parse inputs
  dims = dimensions;      // Tensor dimensions
  gpuID = deviceID;       // Device to use
  label = dataLabel;      // Identifying label for current Tensor
  setLogger(loggerName);  // Set the logger
  pitched = usePitched;   // Flag to use pitched memory
  allocate();             // Allocate the array
  resetToZeros();         // Initialize all values to zero
}

/// The copy constructor allows the definition of a new Tensor using an existing
/// Tensor. By default, the copy will be made on the same device with the same
/// alignment (pitch), but these can be changed using the arguments.
template <typename T>
Tensor<T>::Tensor(const Tensor<T>& A, std::optional<int> deviceID,
                  std::optional<std::string> copyLabel,
                  std::optional<std::string> loggerName,
                  std::optional<bool> usePitched) {
  dims = A.getDimensions();
  gpuID = deviceID ? *deviceID : A.getDeviceID();
  label = copyLabel ? *copyLabel : A.getLabel() + "_copy";
  setLogger(loggerName ? *loggerName : A.getLogger());
  // logtrace("Calling copy constructor from {} to {}.", A.devstr(), devstr());
  pitched = usePitched ? *usePitched : A.isPitched();
  allocate();
  copyFrom(A.data());
}

/// This explicit move-assignment operator is necessary to avoid accidental
/// double-freeing of ptr.
template <typename T>
Tensor<T>& Tensor<T>::operator=(Tensor<T>&& A) {
  std::swap(ptr, A.ptr);
  std::swap(dims, A.dims);
  std::swap(pitch, A.pitch);
  std::swap(gpuID, A.gpuID);
  std::swap(label, A.label);
  std::swap(pitched, A.pitched);
  std::swap(logName, A.logName);
  setLogger(logName);
  // logtrace("Calling move-assignment operator on {}.", label);
  return *this;
}

template <typename T>
Tensor<T>::~Tensor() {
  if (ptr != nullptr) {
    // Destructor destroys any dynamically allocated objects
    if (gpuID >= 0) {  // If using a GPU
      CCE(cudaSetDevice(gpuID));
      CCE(cudaFree(ptr));
    } else {  // If not using a GPU
      free(ptr);
    }
    loginfo("Destroyed Tensor.", label);
  }
}

/// Reshaping a Tensor only changes its dimensional representation and does not
/// change the underlying data. The new specified dimensions must have the same
/// number of overall elements as the Tensor.
template <typename T>
void Tensor<T>::reshape(const std::vector<size_t>& newDims) {
  if (newDims.empty()) logerror("Empty dimension vector specified.");
  if (prod(newDims) != prod(dims))
    logerror<std::invalid_argument>("Dimensions {} cannot be reshaped into {}.",
                                    vecString(dims), vecString(newDims));
  if (pitched && newDims[0] != dims[0])
    logerror<std::invalid_argument>(
        "Pitched arrays must keep their first dimension when reshaping.");
  loginfo("Reshaping from {} to {}.", vecString(dims), vecString(newDims));
  dims = newDims;
}

template <typename T>
void Tensor<T>::setLogger(std::string loggerName) {
  logName = loggerName;
  if (logName.empty()) {
    slog = spdlog::default_logger();
  } else {
    slog = spdlog::get(logName);
    if (slog == nullptr) {
      slog = spdlog::default_logger();
      logwarn("Logger name {} not found. Using default logger.", logName);
    }
  }
}

template <typename T>
std::string Tensor<T>::vecStringPitched() const {
  std::string s = "(";
  if (!dims.empty()) {
    s += fmt::format("{}", dims[0]);
    if (gpuID && pitched) s += fmt::format("[{:d}]", pitch);
    for (auto it = std::next(dims.begin()); it != dims.end(); ++it) {
      s += fmt::format(", {}", *it);
    }
  }
  s += ")";
  return s;
}

template <typename T>
void Tensor<T>::allocate() {
  if (gpuID >= 0) {  // On the GPU
    CCE(cudaSetDevice(gpuID));
    cudaDeviceProp props;
    CCE(cudaGetDeviceProperties(&props, gpuID));
    if (pitched) {  // Allocate a pitched array
      size_t pitchInBytes;
      CCE(cudaMallocPitch((void**)&ptr, &pitchInBytes, sizeof(T) * dims[0],
                          prod(dims) / dims[0]));
      pitch = pitchInBytes / sizeof(T);
      loginfo("Allocated pitched memory of size {:s} on {:s} at {}.",
              vecStringPitched(), props.name, fmt::ptr(ptr));
    } else {  // Allocate linear memory
      CCE(cudaMalloc((void**)&ptr, sizeof(T) * prod(dims)));
      pitch = dims[0];
      loginfo("Allocated linear memory of size {:s} on {:s} at {}.",
              vecString(dims), props.name, fmt::ptr(ptr));
    }
  } else {  // On the CPU, no padding is necessary.
    ptr = (T*)malloc(sizeof(T) * prod(dims));
    pitch = dims[0];
    loginfo("Allocated linear memory of size {:s} on CPU at {}.",
            vecString(dims), fmt::ptr(ptr));
  }
}

template <typename T>
void Tensor<T>::checkInit() {
  if (empty()) logerror<std::invalid_argument>("Tensor is empty.");
  if (!valid()) logerror("Tensor is not initialized properly.");
}

template <typename T>
void Tensor<T>::resetToZeros() {
  int nbytes = getSizeInBytes();
  if (gpuID >= 0) {
    CCE(cudaSetDevice(gpuID));
    CCE(cudaMemset(ptr, 0, nbytes));
  } else {
    memset(ptr, 0, nbytes);
  }
  logdebug("Reset {} bytes at {} to zero.", nbytes, fmt::ptr(ptr));
}

template <typename T>
void Tensor<T>::resetToZerosAsync(cudaStream_t stream) {
  int nbytes = getSizeInBytes();
  if (gpuID) {
    CCE(cudaMemsetAsync(ptr, 0, nbytes, stream));
    logdebug("Reset {} bytes at {} to zero (async).", nbytes, fmt::ptr(ptr));
  } else {
    resetToZeros();
  }
}

template <typename T>
void Tensor<T>::copyFrom(const T* src, size_t pitchInBytes) {
  checkInit();
  logdebug("Copying {} bytes from {} to {} memory {}.", sizeof(T) * prod(dims),
           fmt::ptr(src), devstr(), fmt::ptr(ptr));
  // If omitted, assume the pitch matches the first data dimension
  if (pitchInBytes == 0) pitchInBytes = sizeof(T) * dims[0];
  CCE(cudaMemcpy2D(ptr, sizeof(T) * pitch, src, pitchInBytes,
                   sizeof(T) * dims[0], prod(dims) / dims[0],
                   cudaMemcpyDefault));
}

template <typename T>
void Tensor<T>::copyFromAsync(const T* src, cudaStream_t stream,
                              size_t pitchInBytes) {
  checkInit();
  logdebug("Copying {} bytes from {} to {} memory {} (async).",
           sizeof(T) * prod(dims), fmt::ptr(src), devstr(), fmt::ptr(ptr));
  // If omitted, assume the pitch matches the first data dimension
  if (pitchInBytes == 0) pitchInBytes = sizeof(T) * dims[0];
  CCE(cudaMemcpy2DAsync(ptr, sizeof(T) * pitch, src, pitchInBytes,
                        sizeof(T) * dims[0], prod(dims) / dims[0],
                        cudaMemcpyDefault, stream));
}

template <typename T>
void Tensor<T>::copyTo(T* dst, size_t pitchInBytes) {
  checkInit();
  logdebug("Copying {} bytes from {} memory {} to {}.", sizeof(T) * prod(dims),
           devstr(), fmt::ptr(ptr), fmt::ptr(dst));
  // If omitted, assume the pitch matches the first data dimension
  if (pitchInBytes == 0) pitchInBytes = sizeof(T) * dims[0];
  CCE(cudaMemcpy2D(dst, pitchInBytes, ptr, sizeof(T) * pitch,
                   sizeof(T) * dims[0], prod(dims) / dims[0],
                   cudaMemcpyDefault));
}

template <typename T>
void Tensor<T>::copyToAsync(T* dst, cudaStream_t stream, size_t pitchInBytes) {
  checkInit();
  logdebug("Copying {} bytes from {} memory {} to {} (async).",
           sizeof(T) * prod(dims), devstr(), fmt::ptr(ptr), fmt::ptr(dst));
  // If omitted, assume the pitch matches the first data dimension
  if (pitchInBytes == 0) pitchInBytes = sizeof(T) * dims[0];
  CCE(cudaMemcpy2DAsync(dst, pitchInBytes, ptr, sizeof(T) * pitch,
                        sizeof(T) * dims[0], prod(dims) / dims[0],
                        cudaMemcpyDefault, stream));
}

template <typename T>
void Tensor<T>::copyFrom(Tensor<T>* src) {
  checkInit();
  src->checkInit();
  auto sdims = src->getDimensions();
  if (dims != sdims)
    logerror<std::invalid_argument>(
        "Can't copy Tensor of dimensions {} into a Tensor of dimensions {}.",
        vecString(sdims), vecString(dims));
  logdebug("Copying {} bytes from {} to {} ({} on {} --> {} on {}).",
           sizeof(T) * prod(dims), src->getLabel(), label,
           fmt::ptr(src->data()), src->devstr(), fmt::ptr(ptr), devstr());
  CCE(cudaMemcpy2D(ptr, sizeof(T) * pitch, src->data(), src->getPitchInBytes(),
                   sizeof(T) * dims[0], prod(dims) / dims[0],
                   cudaMemcpyDefault));
}

template <typename T>
void Tensor<T>::copyFromAsync(Tensor<T>* src, cudaStream_t stream) {
  checkInit();
  src->checkInit();
  auto sdims = src->getDimensions();
  if (dims != sdims)
    logerror<std::invalid_argument>(
        "Can't copy Tensor of dimensions {} into a Tensor of dimensions {}.",
        vecString(sdims), vecString(dims));
  logdebug("Copying {} bytes from {} to {} ({} on {} --> {} on {}).",
           sizeof(T) * prod(dims), src->getLabel(), label,
           fmt::ptr(src->data()), src->devstr(), fmt::ptr(ptr), devstr());
  CCE(cudaMemcpy2DAsync(ptr, sizeof(T) * pitch, src->data(),
                        src->getPitchInBytes(), sizeof(T) * dims[0],
                        prod(dims) / dims[0], cudaMemcpyDefault, stream));
}

template <typename T>
void Tensor<T>::copyTo(Tensor<T>* dst) {
  checkInit();
  dst->checkInit();
  auto ddims = dst->getDimensions();
  if (dims != ddims)
    logerror<std::invalid_argument>(
        "Can't copy Tensor of dimensions {} into a Tensor of dimensions {}.",
        vecString(dims), vecString(ddims));
  logdebug("Copying {} bytes from {} to {} ({} on {} --> {} on {}).",
           sizeof(T) * prod(dims), label, dst->getLabel(), fmt::ptr(ptr),
           devstr(), fmt::ptr(dst->data()), dst->devstr());
  CCE(cudaMemcpy2D(dst->data(), dst->getPitchInBytes(), ptr, sizeof(T) * pitch,
                   sizeof(T) * dims[0], prod(dims) / dims[0],
                   cudaMemcpyDefault));
}

template <typename T>
void Tensor<T>::copyToAsync(Tensor<T>* dst, cudaStream_t stream) {
  checkInit();
  dst->checkInit();
  auto ddims = dst->getDimensions();
  if (dims != ddims)
    logerror<std::invalid_argument>(
        "Can't copy Tensor of dimensions {} into a Tensor of dimensions {}.",
        vecString(dims), vecString(ddims));
  logdebug("Copying {} bytes from {} to {} ({} on {} --> {} on {}).",
           sizeof(T) * prod(dims), label, dst->getLabel(), fmt::ptr(ptr),
           devstr(), fmt::ptr(dst->data()), dst->devstr());
  CCE(cudaMemcpy2DAsync(dst->data(), dst->getPitchInBytes(), ptr,
                        sizeof(T) * pitch, sizeof(T) * dims[0],
                        prod(dims) / dims[0], cudaMemcpyDefault, stream));
}

// template <typename T>
// Tensor<T> Tensor<T>::makeCopy(int deviceID, std::string copyLabel) {
//   if (copyLabel.empty()) copyLabel = label + "_copy";
//   auto copy = Tensor<T>(dims, deviceID, copyLabel, logName, pitched);
//   copyTo(copy.data());
//   return copy;
// }

template class Tensor<short>;
template class Tensor<int>;
template class Tensor<float>;
template class Tensor<std::complex<short>>;
template class Tensor<std::complex<int>>;
template class Tensor<std::complex<float>>;
template class Tensor<cuda::std::complex<short>>;
template class Tensor<cuda::std::complex<int>>;
template class Tensor<cuda::std::complex<float>>;

}  // namespace rtbf
