# rtbf Examples

## matBF and rtbf_mex

For most use cases, `rtbf_mex` will be the main MATLAB-based interface to the code. Below are some examples of usage. Check out the `.mlx` examples to play with the code and some real data in the MATLAB Live notebooks.

These examples will grow in complexity as we go along.

### Warming up

Let's start with a simple Hilbert transform along the first dimension.

```MATLAB
rfdata = randn(200, 100, 20, 'single');  % Create dummy data
H.operator = 'Hilbert';  % Define a Hilbert operator using a MATLAB struct
iqdata = rtbf_mex(rfdata, H);  % Outputs complex single array [200, 100, 20]
```

Done! Now `iqdata` contains the Hilbert transformed version of `rfdata`. The struct `H` defines the operation to be applied to `rfdata`, which in this case was `'Hilbert'`. As we will see, `rtbf_mex` uses these MATLAB structs to define the order of operators.

### Combining operators

Now, let's apply both a Hilbert transform and a channel sum to the last dimension of the data. We will reuse `rfdata` and `H`, and define a channel sum operator `C`:

```MATLAB
C.operator = 'ChannelSum';
C.axis = -1;  % Sum along the last axis. 0-indexed for now...
iqsum = rtbf_mex(rfdata, H, C);  % Outputs complex single array [200, 100, 1]
```

The output of `H` is assumed to be the input to `C`. (It is also possible to manually specify input and output arrays, if desired.) You can combine however many operators you'd like by appending more MATLAB structs to the call.

### Reusing graphs for real-time rates

Once the graph has been instantiated, future calls only need to supply the new `rfdata` to beamform at super fast rates:

```MATLAB
rfdata_new = randn(200, 100, 20, 'single');
iqnew = rtbf_mex(rfdata_new);
```

These calls run extremely quickly because the underlying C++ code knows to hand the output of `H` to the next operator `C`.

Note that the new input data should have identical dimensions to the original input data. You will receive an error if the new data does not match the size of the existing graph.

### Multiple concurrent graphs

By default, operators are appended to a graph named "default". A different graph can be selected by providing a name as the first argument:

```MATLAB
iqsum2 = rtbf_mex('another_graph', rfdata, C, H);  % Define new graph
iqsum1 = rtbf_mex(rfdata);  % We can still use graph "default"...
iqsum2 = rtbf_mex('another_graph', rfdata);  % ... alongside the new graph
```

This feature is useful when you have two independent beamforming processes that you'd like to run on the same GPU. Observe in this example that `iqsum1` should equal `iqsum2` (within numerical precision) due to the linearity of the operators.

### Beamforming

Probably the most ubiquitous task is delay-and-sum beamforming. Here, we will require the `Focus` and `Bmode` objects to apply time delays and envelope detection, respectively. To beamform raw data, we will first need a little more information about the data acquisition itself. Let us assume we have some real data stored in `rfdata` with size `[nsamps, nxmits, nchans]`, where each transmit had a focus located at `vspos` (virtual source position). We can easily achieve virtual source beamforming as

```MATLAB
% Define the Focus operator
F.operator = 'Focus';
F.pxpos = pxpos;  % Should have size [3, ...]
F.elpos = elpos;  % Should have size [3, nelems]
F.txdat = vspos;  % Should have size [3, nxmits]
F.fs = fs;  % Sampling frequency, scalar
F.fc = fc;  % Center frequency, scalar
F.c0 = c0;  % Sound speed, scalar
F.time0 = t0;  % Time zero of the acquisition
F.txbf_mode = 'SUM';  % Sum the transmits.
F.rxbf_mode = 'IDENTITY';  % Do not sum the receives; leave them as is.
F.txfnum = 1;  % Desired transmit f-number
F.rxfnum = 1;  % Desired receive f-number
% Define the Bmode operator
B.operator = 'Bmode';
B.compression = 'log';  % Alternatively 'power', which has parameter B.gamma
% Execute the whole graph
bimg = rtbf_mex(rfdata, F, C, H, B);  % Focus->ChannelSum->Hilbert->Bmode
% Real-time execution
bimg = rtbf_mex(rfdata);  % Using new rfdata
```

### Retrieving intermediate outputs

We can also output the intermediate outputs. The outputs are provided in reverse order of the operators. For the above example, we can get just the output of the `Bmode`, or include the output of the `Hilbert`, `ChannelSum`, and `Focus` operators as desired:

```MATLAB
% All of these calls are valid!
bimg = rtbf_mex(rfdata, F, C, H, B);
[bimg, hilb] = rtbf_mex(rfdata);             % Outputs of B, H
[bimg, hilb, csum] = rtbf_mex(rfdata);       % Outputs of B, H, C
[bimg, hilb, csum, foc] = rtbf_mex(rfdata);  % Outputs of B, H, C, F
whos bimg hilb csum foc
```

Note: the GPU to CPU transfer is slow, so avoid grabbing all of these outputs inside of a real-time imaging loop.

## Compiling your own C++ executables

*Coming soon...*
