/**
 @file bmode/rtbf_mex.cu
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2022-05-01

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "rtbf_mex.cuh"

void MexFunction::addTensor(GraphData &g, mdat::Array arr, std::string name) {
  mdat::apply_visitor(
      arr,
      overloaded{
          [&](mdat::TypedArray<short> &&a) { addTensor<short>(g, a, name); },
          [&](mdat::TypedArray<float> &&a) { addTensor<float>(g, a, name); },
          [&](mdat::TypedArray<sshort> &&a) { addTensor<sshort>(g, a, name); },
          [&](mdat::TypedArray<sfloat> &&a) { addTensor<sfloat>(g, a, name); },
          [&](auto &&a) {
            printErr(
                "Tensor must have type short, float, complex<short>, or "
                "complex<float>.");
          }});
}

template <typename T>
void MexFunction::addTensor(GraphData &graph, mdat::TypedArray<T> arr,
                            std::string name) {
  // Get a non-owning TensorView of MATLAB array
  if constexpr (std::is_same_v<T, sshort>) {
    auto arr_t = getTensor<cshort>(arr);
    auto ptr = std::make_shared<Tensor<cshort>>(arr_t, gpuID, name);
    addTensor(graph, ptr, name);
  } else if (std::is_same_v<T, sfloat>) {
    auto arr_t = getTensor<cfloat>(arr);
    auto ptr = std::make_shared<Tensor<cfloat>>(arr_t, gpuID, name);
    addTensor(graph, ptr, name);
  } else {
    auto arr_t = getTensor<T>(arr);
    auto ptr = std::make_shared<Tensor<T>>(arr_t, gpuID, name);
    addTensor(graph, ptr, name);
  }
}

template <typename T>
void MexFunction::addTensor(GraphData &graph, std::shared_ptr<Tensor<T>> a,
                            std::string name) {
  // Ensure a unique name for this graph
  if (name.empty() || graph.tns.count(name)) {
    int i = 0;
    if (name.empty()) name = "tensor";
    while (graph.tns.count(name + "_" + std::to_string(i))) i++;
    name = name + "_" + std::to_string(i);
  }
  printMsg("Adding tensor \"{}\" of datatype {} with dimensions {}.", name,
           type_name<T>(), vecString(a->getDimensions()));
  graph.tns[name] = std::move(a);
}

void MexFunction::addInputLoaderToGraph(GraphData &graph,
                                        const mdat::Array &arr,
                                        std::vector<size_t> dims) {
  std::string name = "InputLoader";
  std::string out_name = "input";
  if (dims.empty()) dims = arr.getDimensions();
  cudaStream_t &s = graph.stream;
  auto cmd = [](auto &&p) {};
  mdat::apply_visitor(
      arr, overloaded{[=, &graph, &s](mdat::TypedArray<short> &&a) {
                        addOperator<InputLoader, short>(
                            graph, name, out_name, cmd,
                            std::shared_ptr<Tensor<short>>(), dims, gpuID, s);
                      },
                      [=, &graph, &s](mdat::TypedArray<float> &&a) {
                        addOperator<InputLoader, float>(
                            graph, name, out_name, cmd,
                            std::shared_ptr<Tensor<float>>(), dims, gpuID, s);
                      },
                      [=, &graph, &s](mdat::TypedArray<sshort> &&a) {
                        addOperator<InputLoader, cshort>(
                            graph, name, out_name, cmd,
                            std::shared_ptr<Tensor<cshort>>(), dims, gpuID, s);
                      },
                      [=, &graph, &s](mdat::TypedArray<sfloat> &&a) {
                        addOperator<InputLoader, cfloat>(
                            graph, name, out_name, cmd,
                            std::shared_ptr<Tensor<cfloat>>(), dims, gpuID, s);
                      },
                      [&](auto &&a) {
                        printErr(
                            "Input must have type INT16, SINGLE, "
                            "COMPLEX_INT16, or COMPLEX_SINGLE. Found type {}.",
                            type_name(a.getType()));
                      }});
}

void MexFunction::addOperatorHelper(GraphData &graph,
                                    const mdat::StructArray &S,
                                    std::string &name, TensorType &input,
                                    std::string &out_name) {
  // Get a unique name for the operator
  name = getOptString(S, "name", getReqString(S, "operator"));
  if (graph.ops.count(name)) {
    int i = 0;
    while (graph.ops.count(name + "_" + std::to_string(i))) i++;
    name = name + "_" + std::to_string(i);
  }
  // Get the input Tensor by name
  std::string in_name = getOptString(S, "inputTensor", graph.lastTensor);
  if (graph.tns.count(in_name) == 0)
    printErr("\"{}\": Requested input Tensor \"{}\" is not in graph \"{}\".",
             name, in_name, graph.name);
  input = graph.tns[in_name];
  // Get the output Tensor's desired name
  out_name = getOptString(S, "outputTensor");
  if (out_name.empty()) out_name = name + "->out";
}

void MexFunction::addVSXFormatterToGraph(GraphData &graph,
                                         const mdat::StructArray &S) {
  std::string name, output_name;
  TensorType input;
  addOperatorHelper(graph, S, name, input, output_name);
  cudaStream_t &s = graph.stream;
  // Load VSXFormatter arguments
  size_t nsamps = getReqScalar(S, "nsamps");
  size_t nxmits = getReqScalar(S, "nxmits");
  size_t nelems = getReqScalar(S, "nelems");
  size_t nchans = getReqScalar(S, "nchans");
  size_t nframes = getOptScalar(S, "nframes", 1);
  size_t npulses = getOptScalar(S, "npulses", 1);
  auto vsxMode = getVSXSampleMode(getReqString(S, "sampleMode"));
  auto chmap = getOptTensorWithDims<int>(S, "chanmap", {nchans, nxmits});
  // Get executable function
  auto cmd = [&](auto &&x) {
    // if constexpr to avoid compiler errors when Operator != VSXFormatter.
    if constexpr (is_VSXFormatter<std::decay_t<decltype(*x)>>::value)
      x->formatRawVSXData();
  };
  // Add a VSXFormatter operator
  std::visit(overloaded{[&](std::shared_ptr<Tensor<short>> &p) {
                          addOperator<VSXFormatter>(
                              graph, name, output_name, cmd, p, nsamps, nxmits,
                              nelems, nchans, vsxMode, std::nullopt, npulses,
                              nframes, chmap, gpuID, s);
                        },
                        [&](auto &&p) {
                          printErr(
                              "Expected VSXFormatter's input to have type "
                              "short. Found type {}.",
                              type_name<std::decay_t<decltype(p->val)>>());
                        }},
             input);
  graph.focus_baseband = vsxMode != VSXSampleMode::HILBERT;
  if (vsxMode == VSXSampleMode::BS50BW) {
    graph.vsx_dsf = 8;
  } else if (vsxMode == VSXSampleMode::BS100BW) {
    graph.vsx_dsf = 4;
  } else if (vsxMode == VSXSampleMode::NS200BW) {
    graph.vsx_dsf = 2;
  } else {
    graph.vsx_dsf = 1;
  }
}

void MexFunction::addHilbertToGraph(GraphData &graph,
                                    const mdat::StructArray &S) {
  // Load Hilbert arguments
  std::string name, output_name;
  TensorType input;
  addOperatorHelper(graph, S, name, input, output_name);
  cudaStream_t &s = graph.stream;
  // Get executable function
  auto cmd = [](auto &&x) {
    // if constexpr to avoid compiler errors when Operator != Hilbert.
    if constexpr (is_Hilbert<std::decay_t<decltype(*x)>>::value) x->transform();
  };
  auto fn = [=, &graph, &s](auto &&p) {
    addOperator<Hilbert>(graph, name, output_name, cmd, p, s);
  };
  // Add a Hilbert node to ops using most recent Operator's output as input
  std::visit(overloaded{[&](std::shared_ptr<Tensor<short>> &p) { fn(p); },
                        [&](std::shared_ptr<Tensor<float>> &p) { fn(p); },
                        [&](auto &&p) {
                          printErr(
                              "Expected Hilbert's input to have type short or "
                              "float. Found type {}.",
                              type_name<std::decay_t<decltype(p->val)>>());
                        }},
             input);
}

void MexFunction::addFocusToGraph(GraphData &graph,
                                  const mdat::StructArray &S) {
  std::string name, output_name;
  TensorType input;
  addOperatorHelper(graph, S, name, input, output_name);
  cudaStream_t &s = graph.stream;
  // Load Focus arguments
  auto dims = getDimensions(input);
  auto pxpos = getReqTensor<float>(S, "pxpos");
  auto elpos = getReqTensorWithDims<float>(S, "elpos", {3, dims[2]});
  auto time0 = getReqTensorWithDims<float>(S, "time0", {dims[1]});
  auto txdat = getReqTensor<float>(S, "txdat");
  float fs = getReqScalar(S, "fs");  // / graph.vsx_dsf;
  // printMsg("Original fs: {}. New fs: {}.", getReqScalar(S, "fs"), fs);
  float fc = getReqScalar(S, "fc");
  float c0 = getReqScalar(S, "c0");
  auto txmode = getOptString(S, "txbf_mode");
  auto rxmode = getOptString(S, "rxbf_mode");
  float txfn = getOptScalar(S, "txfnum", 2);
  float rxfn = getOptScalar(S, "rxfnum", 2);
  bool inputBB = graph.focus_baseband;
  bool outputBB = graph.focus_baseband;
  if (fc == 0.f) {
    fc = getReqScalar(S, "fdemod");
    inputBB = true;
  }
  auto tdims = txdat.getDimensions();
  if (tdims.size() != 2 || (tdims[0] != 2 && tdims[0] != 3) ||
      tdims[1] != dims[1])
    printErr(
        "Focus: Invalid transmit description txdat. Should have size [2 or 3, "
        "{}] but has size {}.",
        dims[1], vecString(tdims));

  BFMode txbfMode = BFMode::SUM;
  BFMode rxbfMode = BFMode::IDENTITY;
  if (txmode.compare("BLKDIAG") == 0)
    txbfMode = BFMode::BLKDIAG;
  else if (txmode.compare("SUM") == 0)
    txbfMode = BFMode::SUM;
  else if (txmode.compare("IDENTITY") == 0)
    txbfMode = BFMode::IDENTITY;
  else
    printErr(
        "Unrecognized transmit beamforming mode. Received '{}' but expected "
        "'IDENTITY', 'SUM', or 'BLKDIAG'.",
        txmode);

  if (rxmode.compare("BLKDIAG") == 0)
    rxbfMode = BFMode::BLKDIAG;
  else if (rxmode.compare("SUM") == 0)
    rxbfMode = BFMode::SUM;
  else if (rxmode.compare("IDENTITY") == 0)
    rxbfMode = BFMode::IDENTITY;
  else
    printErr(
        "Unrecognized receive beamforming mode. Received '{}' but expected "
        "'IDENTITY', 'SUM', or 'BLKDIAG'.",
        rxmode);

  InterpMode iMode = InterpMode::CUBIC;
  auto cmd = [](auto &&x) {
    // if constexpr to avoid compiler errors when Operator != FocusLUT.
    if constexpr (is_Focus<std::decay_t<decltype(*x)>>::value) x->focus();
  };
  // In C++20, we will be able to used templated lambda functions to fold the RF
  // and IQ cases into a single lambda function.
  auto fnRF = [=, &graph, &s](auto &&p) {
    addOperator<Focus, float>(graph, name, output_name, cmd, p, &pxpos, &elpos,
                              &txdat, &time0, fs, fc, false, false, txfn, rxfn,
                              c0, txbfMode, rxbfMode, iMode, s);
  };
  auto fnIQ = [=, &graph, &s](auto &&p) {
    addOperator<Focus, cfloat>(graph, name, output_name, cmd, p, &pxpos, &elpos,
                               &txdat, &time0, fs, fc, inputBB, outputBB, txfn,
                               rxfn, c0, txbfMode, rxbfMode, iMode, s);
  };
  std::visit(overloaded{[&](std::shared_ptr<Tensor<float>> &p) { fnRF(p); },
                        [&](std::shared_ptr<Tensor<cfloat>> &p) { fnIQ(p); },
                        [&](auto &&p) {
                          printErr(
                              "Expected Focus's input to have type float or "
                              "complex<float>. Found type {}.",
                              type_name<std::decay_t<decltype(p->val)>>());
                        }},
             input);
}
void MexFunction::addFocusLUTToGraph(GraphData &graph,
                                     const mdat::StructArray &S) {
  std::string name, output_name;
  TensorType input;
  addOperatorHelper(graph, S, name, input, output_name);
  cudaStream_t &s = graph.stream;
  // Load FocusLUT arguments
  Tensor<float> txdel = getReqTensor<float>(S, "txdel");
  Tensor<float> rxdel = getReqTensor<float>(S, "rxdel");
  // auto txapo = getOptTensor<float>(S, "txapo"); // TODO: Implement
  // auto rxapo = getOptTensor<float>(S, "rxapo"); // TODO: Implement
  float dz = getReqScalar(S, "dz");
  float fs = getReqScalar(S, "fs") / graph.vsx_dsf;
  float fc = getReqScalar(S, "fc");
  float c0 = getReqScalar(S, "c0");
  bool inputBB = graph.focus_baseband;
  bool outputBB = graph.focus_baseband;
  if (fc == 0.f) {
    fc = getReqScalar(S, "fdemod");
    inputBB = true;
  }
  rtbf::BFMode txbfMode = rtbf::BFMode::SUM;       // TODO: Remove hard code
  rtbf::BFMode rxbfMode = rtbf::BFMode::IDENTITY;  // TODO: Remove hard code
  InterpMode iMode = InterpMode::CUBIC;
  auto cmd = [](auto &&x) {
    // if constexpr to avoid compiler errors when Operator != Focus.
    if constexpr (is_FocusLUT<std::decay_t<decltype(*x)>>::value) x->focus();
  };
  // In C++20, we will be able to used templated lambda functions to fold the
  // RF and IQ cases into a single lambda function.
  auto fnRF = [=, &graph, &s](auto &&p) {
    addOperator<FocusLUT, float>(graph, name, output_name, cmd, p, &txdel,
                                 &rxdel, nullptr, nullptr, fs, fc, c0, false,
                                 false, dz, txbfMode, rxbfMode, iMode, s);
  };
  auto fnIQ = [=, &graph, &s](auto &&p) {
    addOperator<FocusLUT, cfloat>(graph, name, output_name, cmd, p, &txdel,
                                  &rxdel, nullptr, nullptr, fs, fc, c0, inputBB,
                                  outputBB, dz, txbfMode, rxbfMode, iMode, s);
  };
  std::visit(overloaded{[&](std::shared_ptr<Tensor<float>> &p) { fnRF(p); },
                        [&](std::shared_ptr<Tensor<cfloat>> &p) { fnIQ(p); },
                        [&](auto &&p) {
                          printErr(
                              "Expected FocusLUT's input to have type float or "
                              "complex<float>. Found type {}.",
                              type_name<std::decay_t<decltype(p->val)>>());
                        }},
             input);
}
void MexFunction::addChannelSumToGraph(GraphData &graph,
                                       const mdat::StructArray &S) {
  std::string name, output_name;
  TensorType input;
  addOperatorHelper(graph, S, name, input, output_name);
  cudaStream_t &s = graph.stream;
  int axis = getOptScalar(S, "axis", -1);
  // Load ChannelSum arguments
  auto cmd = [](auto &&x) {
    // if constexpr to avoid compiler errors when Operator != ChannelSum.
    if constexpr (is_ChannelSum<std::decay_t<decltype(*x)>>::value)
      x->sumChannels();
  };
  std::visit(
      overloaded{
          [&, name, output_name, cmd](std::shared_ptr<Tensor<short>> &p) {
            addOperator<ChannelSum, short>(graph, name, output_name, cmd, p,
                                           axis, 1, s);
          },
          [&, name, output_name, cmd](std::shared_ptr<Tensor<float>> &p) {
            addOperator<ChannelSum, float>(graph, name, output_name, cmd, p,
                                           axis, 1, s);
          },
          [&, name, output_name, cmd](std::shared_ptr<Tensor<cshort>> &p) {
            addOperator<ChannelSum, cshort>(graph, name, output_name, cmd, p,
                                            axis, 1, s);
          },
          [&, name, output_name, cmd](std::shared_ptr<Tensor<cfloat>> &p) {
            addOperator<ChannelSum, cfloat>(graph, name, output_name, cmd, p,
                                            axis, 1, s);
          },
          [&](auto &&p) {
            printErr(
                "Expected ChannelSum's input to have type short, float, "
                "complex<short>, or complex<float>. Found type {}.",
                type_name<std::decay_t<decltype(p->val)>>());
          }},
      input);
}
void MexFunction::addBmodeToGraph(GraphData &graph,
                                  const mdat::StructArray &S) {
  std::string name, output_name;
  TensorType input;
  addOperatorHelper(graph, S, name, input, output_name);
  cudaStream_t &s = graph.stream;
  // Load Bmode arguments
  auto mode = getOptString(S, "compression");
  float gamma = getOptScalar(S, "gamma", 0.3f);
  auto cmd = [mode, gamma](auto &&x) {
    // if constexpr to avoid compiler errors when Operator != Bmode.
    if constexpr (is_Bmode<std::decay_t<decltype(*x)>>::value) {
      if (mode.compare("log") == 0) {
        x->getEnvelopeLogCompress();
      } else if (mode.compare("power") == 0) {
        x->getEnvelopePowCompress(gamma);
      } else {
        x->getEnvelope();
      }
    }
  };
  auto fn = [=, &graph, &s](auto &&p) {
    addOperator<Bmode, float>(graph, name, output_name, cmd, p, std::nullopt,
                              s);
  };
  std::visit(
      overloaded{[&](std::shared_ptr<Tensor<cshort>> &p) { fn(p); },
                 [&](std::shared_ptr<Tensor<cfloat>> &p) { fn(p); },
                 [&](auto &&p) {
                   printErr(
                       "Expected Bmode's input to have type complex<short> or "
                       "complex<float>. Found type {}.",
                       type_name<std::decay_t<decltype(p->val)>>());
                 }},
      input);
}

void MexFunction::addRefocusToGraph(GraphData &graph,
                                    const mdat::StructArray &S) {
  std::string name, output_name;
  TensorType input;
  addOperatorHelper(graph, S, name, input, output_name);
  cudaStream_t &s = graph.stream;
  // Load Bmode arguments
  auto dims = getDimensions(input);
  if (dims.size() < 2)
    printErr(
        "Expected the input Tensor to Refocus to have dimensions [# samples, # "
        "transmits, # rx elements]. Found dimensions {}.",
        vecString(dims));
  TensorView<cfloat> txinv = getReqTensor<cfloat>(S, "txinv");
  auto idims = txinv.getDimensions();
  if (idims.size() != 3 || idims[1] != dims[1] || idims[2] != dims[0])
    printErr(
        "Expected the decoder Tensor for Refocus to have dimensions [# tx "
        "elements, # transmits, # samples. Found dimensions {}.",
        vecString(idims));
  auto cmd = [](auto &&x) {
    // if constexpr to avoid compiler errors when Operator != ChannelSum.
    if constexpr (is_Refocus<std::decay_t<decltype(*x)>>::value) x->decode();
  };
  auto fn = [=, &graph, &txinv, &s](auto &&p) {
    addOperator<Refocus>(graph, name, output_name, cmd, p, &txinv, s);
  };
  std::visit(overloaded{[&](std::shared_ptr<Tensor<cfloat>> &p) { fn(p); },
                        [&](auto &&p) {
                          printErr(
                              "Expected Refocus's input to have type "
                              "complex<float>. Found type {}.",
                              type_name<std::decay_t<decltype(p->val)>>());
                        }},
             input);
}

template <typename Tc>
void MexFunction::loadTensor(Tensor<Tc> *t, mdat::Array a,
                             const cudaStream_t &s) {
  mdat::apply_visitor(
      a, overloaded{
             [&](mdat::TypedArray<short> &&m) { loadTensor<short>(t, m, s); },
             [&](mdat::TypedArray<float> &&m) { loadTensor<float>(t, m, s); },
             [&](mdat::TypedArray<sshort> &&m) { loadTensor<sshort>(t, m, s); },
             [&](mdat::TypedArray<sfloat> &&m) { loadTensor<sfloat>(t, m, s); },
             [&](auto &&a) {
               printErr(
                   "Tensor must have type short, float, complex<short>, or "
                   "complex<float>.");
             }});
}
template <typename Tm, typename Tc>
void MexFunction::loadTensor(Tensor<Tc> *t, mdat::TypedArray<Tm> m,
                             const cudaStream_t &s) {
  std::vector<size_t> tdims = t->getDimensions();
  std::vector<size_t> mdims = m.getDimensions();
  bool isvalid = true;
  if (tdims.empty() || mdims.empty() || tdims[0] > mdims[0]) isvalid = false;
  int ntdims = 0, nmdims = 0;

  for (int i = 0; i < tdims.size(); i++) ntdims += tdims[i] > 1;
  for (int i = 0; i < mdims.size(); i++) nmdims += mdims[i] > 1;
  if (ntdims != nmdims) isvalid = false;
  for (int i = 1; i < ntdims; i++)
    if (tdims[i] > 1 && tdims[i] != mdims[i]) isvalid = false;
  if (!isvalid)
    printErr(
        "Cannot load a tensor of dimensions {} with a MATLAB array of "
        "dimensions {}.",
        vecString(tdims), vecString(mdims));
  // Get a non-owning TensorView of MATLAB array
  if constexpr (std::is_same_v<Tc, Tm>) {
    t->copyFromAsync(getDataPtr<Tc>(m), s, sizeof(Tc) * mdims[0]);
  } else if (std::is_same_v<Tc, cshort> && std::is_same_v<Tm, sshort>) {
    t->copyFromAsync(
        reinterpret_cast<Tc *>(const_cast<Tm *>(getDataPtr<Tm>(m))), s,
        sizeof(Tc) * mdims[0]);
  } else if (std::is_same_v<Tc, cfloat> && std::is_same_v<Tm, sfloat>) {
    t->copyFromAsync(
        reinterpret_cast<Tc *>(const_cast<Tm *>(getDataPtr<Tm>(m))), s,
        sizeof(Tc) * mdims[0]);
  } else {
    printErr(
        "The supplied MATLAB input of type {} with dimensions {} is "
        "incompatible with the graph's input tensor of type {} with "
        "dimensions {}.",
        type_name<Tm>(), vecString(mdims), type_name<Tc>(), vecString(tdims));
  }
}

void MexFunction::execute(GraphData &graph) {
  for (auto &&fn : graph.fns) {
    (fn)();
  }
}

VSXSampleMode MexFunction::getVSXSampleMode(std::string str) {
  if (str == "NS200BW")
    return VSXSampleMode::NS200BW;
  else if (str == "BS100BW")
    return VSXSampleMode::BS100BW;
  else if (str == "BS50BW")
    return VSXSampleMode::BS50BW;
  else
    return VSXSampleMode::HILBERT;
}
